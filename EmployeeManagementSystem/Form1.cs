﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EmployeeManagementSystem
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();            
        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddEmployee addEmployee = new AddEmployee();
            addEmployee.ShowDialog();
        }

        private void addDepartmentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Modify modifyfrom = new Modify();
            modifyfrom.ShowDialog(); 
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void deparmentManagerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }

        private void managementToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Management manager = new Management();
            manager.ShowDialog();
        }

        private void workForEmployeeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            WorkFor workfor = new WorkFor();
            workfor.ShowDialog();
        }

        private void exitToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void projectControlToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ProjectControl Pcontrol = new ProjectControl();
            Pcontrol.ShowDialog();
        }

        private void exitToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void workOnEmployeeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            WorkOn workon = new WorkOn();
            workon.Show();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void viewAllInfoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ViewEmployee VE = new ViewEmployee();
            VE.ShowDialog();
        }
    }
}
