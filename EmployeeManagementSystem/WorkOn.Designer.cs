﻿namespace EmployeeManagementSystem
{
    partial class WorkOn
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.id_txt = new System.Windows.Forms.TextBox();
            this.pId_comboBox = new System.Windows.Forms.ComboBox();
            this.projectName_txt = new System.Windows.Forms.TextBox();
            this.eid_comboBox = new System.Windows.Forms.ComboBox();
            this.workOnProjectEmployee_txt = new System.Windows.Forms.TextBox();
            this.startDate = new System.Windows.Forms.DateTimePicker();
            this.endDate = new System.Windows.Forms.DateTimePicker();
            this.duration_txt = new System.Windows.Forms.TextBox();
            this.addWork_button = new System.Windows.Forms.Button();
            this.selectPid_comboBox = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.projectNametxt_txt = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.employeeView_dataGridView = new System.Windows.Forms.DataGridView();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.employeeView_dataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(680, 435);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.employeeView_dataGridView);
            this.tabPage1.Controls.Add(this.button1);
            this.tabPage1.Controls.Add(this.projectNametxt_txt);
            this.tabPage1.Controls.Add(this.label10);
            this.tabPage1.Controls.Add(this.label9);
            this.tabPage1.Controls.Add(this.selectPid_comboBox);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(672, 406);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "View Project Running";
            this.tabPage1.UseVisualStyleBackColor = true;
            this.tabPage1.Click += new System.EventHandler(this.tabPage1_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.groupBox1);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(672, 406);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Add WorkOn Project";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.addWork_button);
            this.groupBox1.Controls.Add(this.duration_txt);
            this.groupBox1.Controls.Add(this.endDate);
            this.groupBox1.Controls.Add(this.startDate);
            this.groupBox1.Controls.Add(this.workOnProjectEmployee_txt);
            this.groupBox1.Controls.Add(this.eid_comboBox);
            this.groupBox1.Controls.Add(this.projectName_txt);
            this.groupBox1.Controls.Add(this.pId_comboBox);
            this.groupBox1.Controls.Add(this.id_txt);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(666, 400);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Work On Information";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(180, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "ID : ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(131, 67);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 16);
            this.label2.TabIndex = 0;
            this.label2.Text = "Project ID :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(105, 105);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(110, 16);
            this.label3.TabIndex = 0;
            this.label3.Text = "Project Name :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(110, 140);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(105, 16);
            this.label4.TabIndex = 0;
            this.label4.Text = "Employee ID :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(52, 172);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(163, 16);
            this.label5.TabIndex = 0;
            this.label5.Text = "Who Work On Project :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(130, 239);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(85, 16);
            this.label6.TabIndex = 0;
            this.label6.Text = "Start Date :";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(135, 273);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(80, 16);
            this.label7.TabIndex = 0;
            this.label7.Text = "End Date :";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(141, 305);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(74, 16);
            this.label8.TabIndex = 0;
            this.label8.Text = "Duration :";
            // 
            // id_txt
            // 
            this.id_txt.Location = new System.Drawing.Point(225, 29);
            this.id_txt.Name = "id_txt";
            this.id_txt.ReadOnly = true;
            this.id_txt.Size = new System.Drawing.Size(313, 22);
            this.id_txt.TabIndex = 1;
            // 
            // pId_comboBox
            // 
            this.pId_comboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.pId_comboBox.FormattingEnabled = true;
            this.pId_comboBox.Location = new System.Drawing.Point(225, 64);
            this.pId_comboBox.Name = "pId_comboBox";
            this.pId_comboBox.Size = new System.Drawing.Size(313, 24);
            this.pId_comboBox.TabIndex = 2;
            this.pId_comboBox.SelectedIndexChanged += new System.EventHandler(this.pId_comboBox_SelectedIndexChanged);
            // 
            // projectName_txt
            // 
            this.projectName_txt.Location = new System.Drawing.Point(225, 102);
            this.projectName_txt.Name = "projectName_txt";
            this.projectName_txt.ReadOnly = true;
            this.projectName_txt.Size = new System.Drawing.Size(313, 22);
            this.projectName_txt.TabIndex = 3;
            // 
            // eid_comboBox
            // 
            this.eid_comboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.eid_comboBox.FormattingEnabled = true;
            this.eid_comboBox.Location = new System.Drawing.Point(225, 136);
            this.eid_comboBox.Name = "eid_comboBox";
            this.eid_comboBox.Size = new System.Drawing.Size(313, 24);
            this.eid_comboBox.TabIndex = 4;
            this.eid_comboBox.SelectedIndexChanged += new System.EventHandler(this.eid_comboBox_SelectedIndexChanged);
            // 
            // workOnProjectEmployee_txt
            // 
            this.workOnProjectEmployee_txt.Location = new System.Drawing.Point(225, 172);
            this.workOnProjectEmployee_txt.Multiline = true;
            this.workOnProjectEmployee_txt.Name = "workOnProjectEmployee_txt";
            this.workOnProjectEmployee_txt.Size = new System.Drawing.Size(313, 52);
            this.workOnProjectEmployee_txt.TabIndex = 5;
            // 
            // startDate
            // 
            this.startDate.CustomFormat = "yyyy-MM-dd";
            this.startDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.startDate.Location = new System.Drawing.Point(225, 236);
            this.startDate.Name = "startDate";
            this.startDate.Size = new System.Drawing.Size(313, 22);
            this.startDate.TabIndex = 6;
            // 
            // endDate
            // 
            this.endDate.CustomFormat = "yyyy-MM-dd";
            this.endDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.endDate.Location = new System.Drawing.Point(225, 270);
            this.endDate.Name = "endDate";
            this.endDate.Size = new System.Drawing.Size(313, 22);
            this.endDate.TabIndex = 6;
            this.endDate.ValueChanged += new System.EventHandler(this.endDate_ValueChanged);
            // 
            // duration_txt
            // 
            this.duration_txt.Location = new System.Drawing.Point(225, 304);
            this.duration_txt.Name = "duration_txt";
            this.duration_txt.ReadOnly = true;
            this.duration_txt.Size = new System.Drawing.Size(313, 22);
            this.duration_txt.TabIndex = 7;
            // 
            // addWork_button
            // 
            this.addWork_button.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.addWork_button.Location = new System.Drawing.Point(225, 335);
            this.addWork_button.Name = "addWork_button";
            this.addWork_button.Size = new System.Drawing.Size(116, 38);
            this.addWork_button.TabIndex = 8;
            this.addWork_button.Text = "Add";
            this.addWork_button.UseVisualStyleBackColor = false;
            this.addWork_button.Click += new System.EventHandler(this.addWork_button_Click);
            // 
            // selectPid_comboBox
            // 
            this.selectPid_comboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.selectPid_comboBox.FormattingEnabled = true;
            this.selectPid_comboBox.Location = new System.Drawing.Point(166, 19);
            this.selectPid_comboBox.Name = "selectPid_comboBox";
            this.selectPid_comboBox.Size = new System.Drawing.Size(296, 24);
            this.selectPid_comboBox.TabIndex = 0;
            this.selectPid_comboBox.SelectedIndexChanged += new System.EventHandler(this.selectPid_comboBox_SelectedIndexChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(47, 23);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(113, 16);
            this.label9.TabIndex = 1;
            this.label9.Text = "Select Project :";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(50, 58);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(110, 16);
            this.label10.TabIndex = 1;
            this.label10.Text = "Project Name :";
            // 
            // projectNametxt_txt
            // 
            this.projectNametxt_txt.Location = new System.Drawing.Point(166, 55);
            this.projectNametxt_txt.Name = "projectNametxt_txt";
            this.projectNametxt_txt.ReadOnly = true;
            this.projectNametxt_txt.Size = new System.Drawing.Size(296, 22);
            this.projectNametxt_txt.TabIndex = 2;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.button1.Location = new System.Drawing.Point(166, 87);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(116, 38);
            this.button1.TabIndex = 3;
            this.button1.Text = "View";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // employeeView_dataGridView
            // 
            this.employeeView_dataGridView.AllowUserToAddRows = false;
            this.employeeView_dataGridView.AllowUserToDeleteRows = false;
            this.employeeView_dataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.employeeView_dataGridView.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.employeeView_dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.employeeView_dataGridView.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.employeeView_dataGridView.Location = new System.Drawing.Point(3, 144);
            this.employeeView_dataGridView.Name = "employeeView_dataGridView";
            this.employeeView_dataGridView.ReadOnly = true;
            this.employeeView_dataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.employeeView_dataGridView.Size = new System.Drawing.Size(666, 259);
            this.employeeView_dataGridView.TabIndex = 4;
            // 
            // WorkOn
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(680, 435);
            this.Controls.Add(this.tabControl1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(696, 473);
            this.MinimumSize = new System.Drawing.Size(696, 473);
            this.Name = "WorkOn";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "WorkOn";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.employeeView_dataGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox id_txt;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker endDate;
        private System.Windows.Forms.DateTimePicker startDate;
        private System.Windows.Forms.TextBox workOnProjectEmployee_txt;
        private System.Windows.Forms.ComboBox eid_comboBox;
        private System.Windows.Forms.TextBox projectName_txt;
        private System.Windows.Forms.ComboBox pId_comboBox;
        private System.Windows.Forms.TextBox duration_txt;
        private System.Windows.Forms.Button addWork_button;
        private System.Windows.Forms.TextBox projectNametxt_txt;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox selectPid_comboBox;
        private System.Windows.Forms.DataGridView employeeView_dataGridView;
        private System.Windows.Forms.Button button1;
    }
}