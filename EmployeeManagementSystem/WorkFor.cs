﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EmployeeManagementSystem
{
    public partial class WorkFor : Form
    {
        public WorkFor()
        {
            InitializeComponent();
            fillTotalEmployee();
        }

        private void fillTotalEmployee()
        {
            string connection = "datasource = localhost; port = 3306; username = root; password = root;";
            string query = "SELECT count(eid) from ems.employee;";
            MySqlConnection con = new MySqlConnection(connection);
            MySqlCommand commmend = new MySqlCommand(query, con);
            try
            {
                con.Open();
                object count = commmend.ExecuteScalar();
                totalEmployee_txt.Text = count.ToString();
                con.Close();
            }
            catch (Exception x)
            {
                MessageBox.Show(x.Message);
            }
        }

        private void Employee_dataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void viewEmployee_button_Click(object sender, EventArgs e)
        {
            if(this.dept_comboBox.Text == "C")
            {
                string query = "Select eid as Employee_ID, name as Name, sex as Gender, contacts as Phone_No, salary as Salary, photo as Image from ems.employee where c = 'True'";
                ViewDeptWiseEmployee(query);
            }
            else if (this.dept_comboBox.Text == "C++")
            {
                string query = "Select eid as Employee_ID, name as Name, sex as Gender, contacts as Phone_No, salary as Salary, photo as Image from ems.employee where cpluse = 'True'";
                ViewDeptWiseEmployee(query);
            }
            else if (this.dept_comboBox.Text == "C#")
            {
                string query = "Select eid as Employee_ID, name as Name, sex as Gender, contacts as Phone_No, salary as Salary, photo as Image from ems.employee where csharp = 'True'";
                ViewDeptWiseEmployee(query);
            }
            else if (this.dept_comboBox.Text == "PHP")
            {
                string query = "Select eid as Employee_ID, name as Name, sex as Gender, contacts as Phone_No, salary as Salary, photo as Image from ems.employee where php = 'True'";
                ViewDeptWiseEmployee(query);
            }
            else if (this.dept_comboBox.Text == "JAVA")
            {
                string query = "Select eid as Employee_ID, name as Name, sex as Gender, contacts as Phone_No, salary as Salary, photo as Image from ems.employee where java = 'True'";
                ViewDeptWiseEmployee(query);
            }
            else if (this.dept_comboBox.Text == "WEB DESIGNER")
            {
                string query = "Select eid as Employee_ID, name as Name, sex as Gender, contacts as Phone_No, salary as Salary, photo as Image from ems.employee where webdesigner = 'True'";
                ViewDeptWiseEmployee(query);
            }
            else if (this.dept_comboBox.Text == "ANDROID")
            {
                string query = "Select eid as Employee_ID, name as Name, sex as Gender, contacts as Phone_No, salary as Salary, photo as Image from ems.employee where android = 'True'";
                ViewDeptWiseEmployee(query);
            }
            else
            {
                MessageBox.Show("select What u do ");
            }
        }

        private void ViewDeptWiseEmployee(string query)
        {
            string connection = "datasource = localhost; port = 3306; username = root; password = root;";
            MySqlConnection con = new MySqlConnection(connection);
            MySqlCommand commmend = new MySqlCommand(query, con);

            try
            {
                MySqlDataAdapter dataAdapter = new MySqlDataAdapter();
                dataAdapter.SelectCommand = commmend;
                DataTable dataTable = new DataTable();
                dataAdapter.Fill(dataTable);
                BindingSource bsource = new BindingSource();

                bsource.DataSource = dataTable;
                employeeShow_dataGridView.DataSource = bsource;
                dataAdapter.Update(dataTable);


            }
            catch (Exception x)
            {
                MessageBox.Show(x.Message);
            }
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void employeeShow_dataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }

    }
}
