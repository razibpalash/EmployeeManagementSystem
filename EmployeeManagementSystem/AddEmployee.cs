﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EmployeeManagementSystem
{
    public partial class AddEmployee : Form
    {
        public AddEmployee()
        {
            InitializeComponent();
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private string imagePath;
        private void selectImage_btn_Click(object sender, EventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Filter = "JPG Files(*.jpg)|*.jpg|PNG Files(*.png)|*.png";
            if(dlg.ShowDialog() == DialogResult.OK)
            {
                imagePath = dlg.FileName.ToString();
                pictureBox1.ImageLocation = imagePath;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string con = "datasource = localhost; port = 3306; username = root; password = root;";
            MySqlConnection connecttion = new MySqlConnection(con);
            string query = "select * from ems.employee where eid ='" + this.eid_txt.Text + "';";
            MySqlCommand command = new MySqlCommand(query, connecttion);

            try
            {
                MySqlDataReader Reader;
                connecttion.Open();
                Reader = command.ExecuteReader();
                int counter = 0;
                while (Reader.Read())
                {
                    counter++;
                }
                if (counter == 0)
                {
                    Addemployees();
                }
                else
                {
                    MessageBox.Show("this Employee ID is already exist");
                }
                connecttion.Close();
            }
            catch (Exception x)
            {
                MessageBox.Show(x.Message);
            }
        }

        private string gender;
        private void Addemployees()
        {
            try
            {
                byte[] imageBt = null;
                FileStream fstream = new FileStream(this.imagePath, FileMode.Open, FileAccess.Read);
                BinaryReader br = new BinaryReader(fstream);
                imageBt = br.ReadBytes((int)fstream.Length);

                string con = "datasource = localhost; port = 3306; username = root; password = root;";
                string Query = "insert into ems.employee values('" + this.eid_txt.Text + "','" + this.name_txt.Text + "','" + this.gender + "','" + this.age_txt.Text + "','" + this.address_txt.Text + "','" + this.contacts_txt.Text + "','" + this.dateTimePicker1.Text + "','" + this.c_checkobx.Checked.ToString() + "','" + this.cSharp_checkBox.Checked.ToString() + "','" + this.cPush_checkBox.Checked.ToString() + "','" + this.java_checkBox.Checked.ToString() + "','" + this.php_checkBox.Checked.ToString() + "','" + this.android_checkBox.Checked.ToString() + "','" + this.webdesigner_checkBox.Checked.ToString() + "','" + this.salary_txt.Text + "',@IMG) ;";
                MySqlConnection connection = new MySqlConnection(con);
                MySqlCommand command = new MySqlCommand(Query, connection);
                MySqlDataReader reader2;

            

                connection.Open();
                command.Parameters.Add(new MySqlParameter("@IMG", imageBt));
                reader2 = command.ExecuteReader();
                MessageBox.Show("ADD NEW EMPLOYEE");
                nullToAll();
                
            }
            catch (Exception x)
            {
                MessageBox.Show(x.Message);
            }
        }

        private void nullToAll()
        {
            eid_txt.Text = null;
            name_txt.Text = null;
            radioButton1.Checked = false;
            radioButton2.Checked = false;
            age_txt.Text = null;
            contacts_txt.Text = null;
            address_txt.Text = null;
            c_checkobx.Checked = false;
            cPush_checkBox.Checked = false;
            cSharp_checkBox.Checked = false;
            php_checkBox.Checked = false;
            android_checkBox.Checked = false;
            webdesigner_checkBox.Checked = false;
            java_checkBox.Checked = false;
            salary_txt.Text = null;
            pictureBox1.Image = EmployeeManagementSystem.Properties.Resources.Office_Customer_Male_Light_icon;
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            gender = "Male";
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            gender = "Female";
        }

        private void eid_txt_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;
            if (!char.IsDigit(ch) && ch != 8 && ch != 46)
            {
                e.Handled = true;
            }
        }

        private void age_txt_TextChanged(object sender, EventArgs e)
        {

        }

        private void age_txt_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;
            if (!char.IsDigit(ch) && ch != 8 && ch != 46)
            {
                e.Handled = true;
            }
        }

        private void contacts_txt_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;
            if (!char.IsDigit(ch) && ch != 8 && ch != 46)
            {
                e.Handled = true;
            }
        }

        private void salary_txt_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;
            if (!char.IsDigit(ch) && ch != 8 && ch != 46)
            {
                e.Handled = true;
            }
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            string con = "datasource = localhost; port = 3306; username = root; password = root;";
            MySqlConnection connecttion = new MySqlConnection(con);
            string query = "select * from ems.department where did ='" + this.dId_txt.Text + "';";
            MySqlCommand command = new MySqlCommand(query, connecttion);

            try
            {
                MySqlDataReader Reader;
                connecttion.Open();
                Reader = command.ExecuteReader();
                int counter = 0;
                while (Reader.Read())
                {
                    counter++;
                }
                if (counter == 0)
                {
                    AddDept();
                }
                else
                {
                    MessageBox.Show("this Department ID is already exist");
                }
                connecttion.Close();
            }
            catch (Exception x)
            {
                MessageBox.Show(x.Message);
            }
        }

        private void AddDept()
        {
            string con = "datasource = localhost; port = 3306; username = root; password = root;";
            string Query = "insert into ems.department values('" + this.dId_txt.Text + "','" + this.dept_name_txt.Text + "','" + this.dept_location_txt.Text + "') ;";
            MySqlConnection connection = new MySqlConnection(con);
            MySqlCommand command = new MySqlCommand(Query, connection);
            MySqlDataReader reader;
            
            try
            {
                connection.Open();
                reader = command.ExecuteReader();
                MessageBox.Show("ADD NEW DEPARTMENT");
                connection.Close();
                dId_txt.Text = null;
                dept_name_txt.Text = null;

            }
            catch (Exception x)
            {
                MessageBox.Show(x.Message);
            }
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void AddEmployee_Load(object sender, EventArgs e)
        {

        }

        private void button6_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            string con = "datasource = localhost; port = 3306; username = root; password = root;";
            MySqlConnection connecttion = new MySqlConnection(con);
            string query = "select * from ems.projects where pid ='" + this.projectID_txt.Text + "';";
            MySqlCommand command = new MySqlCommand(query, connecttion);

            try
            {
                MySqlDataReader Reader;
                connecttion.Open();
                Reader = command.ExecuteReader();
                int counter = 0;
                while (Reader.Read())
                {
                    counter++;
                }
                if (counter == 0)
                {
                    AddProjects();
                }
                else
                {
                    MessageBox.Show("this Department ID is already exist");
                }
                connecttion.Close();
            }
            catch (Exception x)
            {
                MessageBox.Show(x.Message);
            }
        }

        private void AddProjects()
        {
            string con = "datasource = localhost; port = 3306; username = root; password = root;";
            string Query = "insert into ems.projects values('" + this.projectID_txt.Text + "','" + this.projectName_txt.Text + "','" + this.comboBox1.Text +"','" + this.projectLocation_txt.Text + "') ;";
            MySqlConnection connection = new MySqlConnection(con);
            MySqlCommand command = new MySqlCommand(Query, connection);
            MySqlDataReader reader;

            try
            {
                connection.Open();
                reader = command.ExecuteReader();
                MessageBox.Show("ADD NEW PROJECT");
                connection.Close();
                projectID_txt.Text = null;
                projectName_txt.Text = null;
                comboBox1.Text = null;
                projectLocation_txt.Text = "Dhaka, Bangladesh ";

            }
            catch (Exception x)
            {
                MessageBox.Show(x.Message);
            }
        }



    }
}
