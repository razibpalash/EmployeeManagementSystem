﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EmployeeManagementSystem
{
    public partial class ViewEmployee : Form
    {
        public ViewEmployee()
        {
            InitializeComponent();
            fillEmployeeDataGreateView();
            fillDeptDataGreateView();
            fillProjectDataGreateView();
            
        }

        private void fillEmployeeDataGreateView()
        {
            string connection = "datasource = localhost; port = 3306; username = root; password = root;";
            string query = "SELECT * from ems.employee;";
            MySqlConnection con = new MySqlConnection(connection);
            MySqlCommand commmend = new MySqlCommand(query, con);

            try
            {
                MySqlDataAdapter dataAdapter = new MySqlDataAdapter();
                dataAdapter.SelectCommand = commmend;
                DataTable dataTable = new DataTable();
                dataAdapter.Fill(dataTable);
                BindingSource bsource = new BindingSource();

                bsource.DataSource = dataTable;
                employee_dataGridView.DataSource = bsource;
                dataAdapter.Update(dataTable);
            }
            catch (Exception x)
            {
                MessageBox.Show(x.Message);
            }
        }

        private void fillDeptDataGreateView()
        {
            string connection = "datasource = localhost; port = 3306; username = root; password = root;";
            string query = "SELECT * from ems.department;";
            MySqlConnection con = new MySqlConnection(connection);
            MySqlCommand commmend = new MySqlCommand(query, con);

            try
            {
                MySqlDataAdapter dataAdapter = new MySqlDataAdapter();
                dataAdapter.SelectCommand = commmend;
                DataTable dataTable = new DataTable();
                dataAdapter.Fill(dataTable);
                BindingSource bsource = new BindingSource();

                bsource.DataSource = dataTable;
                dept_dataGridView.DataSource = bsource;
                dataAdapter.Update(dataTable);
            }
            catch (Exception x)
            {
                MessageBox.Show(x.Message);
            }
        }

        private void fillProjectDataGreateView()
        {
            string connection = "datasource = localhost; port = 3306; username = root; password = root;";
            string query = "SELECT * from ems.projects;";
            MySqlConnection con = new MySqlConnection(connection);
            MySqlCommand commmend = new MySqlCommand(query, con);

            try
            {
                MySqlDataAdapter dataAdapter = new MySqlDataAdapter();
                dataAdapter.SelectCommand = commmend;
                DataTable dataTable = new DataTable();
                dataAdapter.Fill(dataTable);
                BindingSource bsource = new BindingSource();

                bsource.DataSource = dataTable;
                project_dataGridView.DataSource = bsource;
                dataAdapter.Update(dataTable);


            }
            catch (Exception x)
            {
                MessageBox.Show(x.Message);
            }
        }

        private void viewProjec_button_Click(object sender, EventArgs e)
        {
            if (this.projectView_combobox.Text == "Alleasing")
            {
                string query = "SELECT pid as ProjectID, name as ProjectName, department as ProjectDepartment, location as ProjectLocation from ems.projects;";
                viewproject(query);
            }
            else if (this.projectView_combobox.Text == "Descending Order")
            {
                if (this.order_combobox.Text == "By ID")
                {
                    string query = "SELECT pid as ProjectID, name as ProjectName, department as ProjectDepartment, location as ProjectLocation from ems.projects order by pid desc;";
                    viewproject(query);
                }
                else if (this.order_combobox.Text == "By Name")
                {
                    string query = "SELECT pid as ProjectID, name as ProjectName, department as ProjectDepartment, location as ProjectLocation from ems.projects order by name desc;";
                    viewproject(query);
                }
                else
                {
                    MessageBox.Show("select order By");
                }
            }
            else if (this.projectView_combobox.Text == "Ascending Order")
            {
                if (this.order_combobox.Text == "By ID")
                {
                    string query = "SELECT pid as ProjectID, name as ProjectName, department as ProjectDepartment, location as ProjectLocation from ems.projects order by pid;";
                    viewproject(query);
                }
                else if (this.order_combobox.Text == "By Name")
                {
                    string query = "SELECT pid as ProjectID, name as ProjectName, department as ProjectDepartment, location as ProjectLocation from ems.projects order by name;";
                    viewproject(query);
                }
                else
                {
                    MessageBox.Show("select order By");
                }
            }
            else
            {
                MessageBox.Show("select what u do");
            }
            order_combobox.Visible = false;
            projectView_combobox.Text = null;
        }

        private void viewproject(string query)
        {
            string connection = "datasource = localhost; port = 3306; username = root; password = root;";
            MySqlConnection con = new MySqlConnection(connection);
            MySqlCommand commmend = new MySqlCommand(query, con);

            try
            {
                MySqlDataAdapter dataAdapter = new MySqlDataAdapter();
                dataAdapter.SelectCommand = commmend;
                DataTable dataTable = new DataTable();
                dataAdapter.Fill(dataTable);
                BindingSource bsource = new BindingSource();

                bsource.DataSource = dataTable;
                project_dataGridView.DataSource = bsource;
                dataAdapter.Update(dataTable);


            }
            catch (Exception x)
            {
                MessageBox.Show(x.Message);
            }
        }
        
        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
        
        }

        private void projectView_combobox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (projectView_combobox.Text == "Alleasing")
            {
                order_combobox.Visible = false;
            }
            else if (projectView_combobox.Text == "Descending Order" || projectView_combobox.Text == "Ascending Order")
            {
                order_combobox.Text = null;
                order_combobox.Visible = true;
            }
        }

        private void viewDept_button_Click(object sender, EventArgs e)
        {
            if (this.deptView_comboBox.Text == "Alleasing")
            {
                string query = "SELECT did as Depatrment_ID, name as Department_Name, location as Department_Location from ems.department;";
                viewDept(query);
            }
            else if (this.deptView_comboBox.Text == "Descending Order")
            {
                if (this.orderDept_comboBox.Text == "By ID")
                {
                    string query = "SELECT did as Department_ID, name as Department_Name, location as Department_Location from ems.department order by did desc;";
                    viewDept(query);
                }
                else if (this.orderDept_comboBox.Text == "By Name")
                {
                    string query = "SELECT did as Department_ID, name as Department_Name, location as Department_Location from ems.department order by name desc;";
                    viewDept(query);
                }
                else
                {
                    MessageBox.Show("select order By");
                }
            }
            else if (this.deptView_comboBox.Text == "Ascending Order")
            {
                if (this.orderDept_comboBox.Text == "By ID")
                {
                    string query = "SELECT did as Department_ID, name as Departmtn_Name, location as Department_Location from ems.department order by did;";
                    viewDept(query);
                }
                else if (this.orderDept_comboBox.Text == "By Name")
                {
                    string query = "SELECT did as Department_ID, name as Departmtn_Name, location as Department_Location from ems.department order by name;";
                    viewDept(query);
                }
                else
                {
                    MessageBox.Show("select order By");
                }
            }
            else
            {
                MessageBox.Show("select what u do");
            }
            orderDept_comboBox.Visible = false;
            deptView_comboBox.Text = null;
        }

        private void viewDept(string query)
        {
            string connection = "datasource = localhost; port = 3306; username = root; password = root;";
            MySqlConnection con = new MySqlConnection(connection);
            MySqlCommand commmend = new MySqlCommand(query, con);

            try
            {
                MySqlDataAdapter dataAdapter = new MySqlDataAdapter();
                dataAdapter.SelectCommand = commmend;
                DataTable dataTable = new DataTable();
                dataAdapter.Fill(dataTable);
                BindingSource bsource = new BindingSource();

                bsource.DataSource = dataTable;
                dept_dataGridView.DataSource = bsource;
                dataAdapter.Update(dataTable);
            }
            catch (Exception x)
            {
                MessageBox.Show(x.Message);
            }
        }

        private void dept_dataGridView_SelectionChanged(object sender, EventArgs e)
        {
            
        }

        private void deptView_comboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (deptView_comboBox.Text == "Alleasing")
            {
                orderDept_comboBox.Visible = false;
            }
            else if (deptView_comboBox.Text == "Descending Order" || deptView_comboBox.Text == "Ascending Order")
            {
                orderDept_comboBox.Text = null;
                orderDept_comboBox.Visible = true;
            }
        }

        private void viewEmployee_button_Click(object sender, EventArgs e)
        {
            if (this.epmloyeeView_comboBox.Text == "Alleasing")
            {
                string query = "SELECT eid as Employee_ID, name as Employee_Name, sex as Gender, age as Age, address as Address, contacts as Phone_No, dateofjoining as Date_Of_Joining, c as C, csharp as c_Sharp, cpluse as c_pluse_pluse, java as Java, php as PHP, android as Android, webdesigner as Web_Design, salary as Salary, photo as Image from ems.employee;";
                viewEmployee(query);
            }
            else if (this.epmloyeeView_comboBox.Text == "Descending Order")
            {
                if (this.orderEmployee_comboBox.Text == "By ID")
                {
                    string query = "SELECT eid as Employee_ID, name as Employee_Name, sex as Gender, age as Age, address as Address, contacts as Phone_No, dateofjoining as Date_Of_Joining, c as C, csharp as c_Sharp, cpluse as c_pluse_pluse, java as Java, php as PHP, android as Android, webdesigner as Web_Design, salary as Salary, photo as Image from ems.employee order by eid desc;";
                    viewEmployee(query);
                }
                else if (this.orderEmployee_comboBox.Text == "By Name")
                {
                    string query = "SELECT eid as Employee_ID, name as Employee_Name, sex as Gender, age as Age, address as Address, contacts as Phone_No, dateofjoining as Date_Of_Joining, c as C, csharp as c_Sharp, cpluse as c_pluse_pluse, java as Java, php as PHP, android as Android, webdesigner as Web_Design, salary as Salary, photo as Image from ems.employee order by name desc;";
                    viewEmployee(query);
                }
                else
                {
                    MessageBox.Show("select order By");
                }
            }
            else if (this.orderEmployee_comboBox.Text == "Ascending Order")
            {
                if (this.order_combobox.Text == "By ID")
                {
                    string query = "SELECT eid as Employee_ID, name as Employee_Name, sex as Gender, age as Age, address as Address, contacts as Phone_No, dateofjoining as Date_Of_Joining, c as C, csharp as c_Sharp, cpluse as c_pluse_pluse, java as Java, php as PHP, android as Android, webdesigner as Web_Design, salary as Salary, photo as Image from ems.employee order by eid;";
                    viewEmployee(query);
                }
                else if (this.orderEmployee_comboBox.Text == "By Name")
                {
                    string query = "SELECT eid as Employee_ID, name as Employee_Name, sex as Gender, age as Age, address as Address, contacts as Phone_No, dateofjoining as Date_Of_Joining, c as C, csharp as c_Sharp, cpluse as c_pluse_pluse, java as Java, php as PHP, android as Android, webdesigner as Web_Design, salary as Salary, photo as Image from ems.employee order by name;";
                    viewEmployee(query);
                }
                else
                {
                    MessageBox.Show("select order By");
                }
            }
            else
            {
                MessageBox.Show("select what u do");
            }
            orderEmployee_comboBox.Visible = false;
            epmloyeeView_comboBox.Text = null;
        }

        private void viewEmployee(string query)
        {
            string connection = "datasource = localhost; port = 3306; username = root; password = root;";
            MySqlConnection con = new MySqlConnection(connection);
            MySqlCommand commmend = new MySqlCommand(query, con);

            try
            {
                MySqlDataAdapter dataAdapter = new MySqlDataAdapter();
                dataAdapter.SelectCommand = commmend;
                DataTable dataTable = new DataTable();
                dataAdapter.Fill(dataTable);
                BindingSource bsource = new BindingSource();

                bsource.DataSource = dataTable;
                employee_dataGridView.DataSource = bsource;
                dataAdapter.Update(dataTable);


            }
            catch (Exception x)
            {
                MessageBox.Show(x.Message);
            }
        }

        private void epmloyeeView_comboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (epmloyeeView_comboBox.Text == "Alleasing")
            {
                orderEmployee_comboBox.Visible = false;
            }
            else if (epmloyeeView_comboBox.Text == "Descending Order" || epmloyeeView_comboBox.Text == "Ascending Order")
            {
                orderEmployee_comboBox.Text = null;
                orderEmployee_comboBox.Visible = true;
            }
        }


    }    
}
