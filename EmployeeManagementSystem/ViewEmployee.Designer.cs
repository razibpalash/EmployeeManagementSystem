﻿namespace EmployeeManagementSystem
{
    partial class ViewEmployee
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.orderEmployee_comboBox = new System.Windows.Forms.ComboBox();
            this.epmloyeeView_comboBox = new System.Windows.Forms.ComboBox();
            this.viewEmployee_button = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.employee_dataGridView = new System.Windows.Forms.DataGridView();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.viewDept_button = new System.Windows.Forms.Button();
            this.orderDept_comboBox = new System.Windows.Forms.ComboBox();
            this.deptView_comboBox = new System.Windows.Forms.ComboBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.dept_dataGridView = new System.Windows.Forms.DataGridView();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.order_combobox = new System.Windows.Forms.ComboBox();
            this.projectView_combobox = new System.Windows.Forms.ComboBox();
            this.viewProjec_button = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.project_dataGridView = new System.Windows.Forms.DataGridView();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.employee_dataGridView)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dept_dataGridView)).BeginInit();
            this.tabPage3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.project_dataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(680, 435);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.orderEmployee_comboBox);
            this.tabPage1.Controls.Add(this.epmloyeeView_comboBox);
            this.tabPage1.Controls.Add(this.viewEmployee_button);
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(672, 406);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "View Employee";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // orderEmployee_comboBox
            // 
            this.orderEmployee_comboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.orderEmployee_comboBox.FormattingEnabled = true;
            this.orderEmployee_comboBox.Items.AddRange(new object[] {
            "By ID",
            "By Name"});
            this.orderEmployee_comboBox.Location = new System.Drawing.Point(220, 14);
            this.orderEmployee_comboBox.Name = "orderEmployee_comboBox";
            this.orderEmployee_comboBox.Size = new System.Drawing.Size(149, 24);
            this.orderEmployee_comboBox.TabIndex = 3;
            this.orderEmployee_comboBox.Visible = false;
            // 
            // epmloyeeView_comboBox
            // 
            this.epmloyeeView_comboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.epmloyeeView_comboBox.FormattingEnabled = true;
            this.epmloyeeView_comboBox.Items.AddRange(new object[] {
            "Alleasing",
            "Descending Order",
            "Ascending Order"});
            this.epmloyeeView_comboBox.Location = new System.Drawing.Point(8, 14);
            this.epmloyeeView_comboBox.Name = "epmloyeeView_comboBox";
            this.epmloyeeView_comboBox.Size = new System.Drawing.Size(206, 24);
            this.epmloyeeView_comboBox.TabIndex = 2;
            this.epmloyeeView_comboBox.SelectedIndexChanged += new System.EventHandler(this.epmloyeeView_comboBox_SelectedIndexChanged);
            // 
            // viewEmployee_button
            // 
            this.viewEmployee_button.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.viewEmployee_button.Location = new System.Drawing.Point(8, 49);
            this.viewEmployee_button.Name = "viewEmployee_button";
            this.viewEmployee_button.Size = new System.Drawing.Size(150, 38);
            this.viewEmployee_button.TabIndex = 1;
            this.viewEmployee_button.Text = "View Employee";
            this.viewEmployee_button.UseVisualStyleBackColor = false;
            this.viewEmployee_button.Click += new System.EventHandler(this.viewEmployee_button_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.employee_dataGridView);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBox1.Location = new System.Drawing.Point(3, 102);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(666, 301);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // employee_dataGridView
            // 
            this.employee_dataGridView.AllowUserToAddRows = false;
            this.employee_dataGridView.AllowUserToDeleteRows = false;
            this.employee_dataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.employee_dataGridView.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.employee_dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.employee_dataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.employee_dataGridView.Location = new System.Drawing.Point(3, 18);
            this.employee_dataGridView.Name = "employee_dataGridView";
            this.employee_dataGridView.ReadOnly = true;
            this.employee_dataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.employee_dataGridView.Size = new System.Drawing.Size(660, 280);
            this.employee_dataGridView.TabIndex = 0;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.viewDept_button);
            this.tabPage2.Controls.Add(this.orderDept_comboBox);
            this.tabPage2.Controls.Add(this.deptView_comboBox);
            this.tabPage2.Controls.Add(this.groupBox3);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(672, 406);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "View Department";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // viewDept_button
            // 
            this.viewDept_button.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.viewDept_button.Location = new System.Drawing.Point(8, 48);
            this.viewDept_button.Name = "viewDept_button";
            this.viewDept_button.Size = new System.Drawing.Size(140, 38);
            this.viewDept_button.TabIndex = 3;
            this.viewDept_button.Text = "View Department";
            this.viewDept_button.UseVisualStyleBackColor = false;
            this.viewDept_button.Click += new System.EventHandler(this.viewDept_button_Click);
            // 
            // orderDept_comboBox
            // 
            this.orderDept_comboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.orderDept_comboBox.FormattingEnabled = true;
            this.orderDept_comboBox.Items.AddRange(new object[] {
            "By ID",
            "By Name"});
            this.orderDept_comboBox.Location = new System.Drawing.Point(220, 16);
            this.orderDept_comboBox.Name = "orderDept_comboBox";
            this.orderDept_comboBox.Size = new System.Drawing.Size(127, 24);
            this.orderDept_comboBox.TabIndex = 2;
            this.orderDept_comboBox.Visible = false;
            // 
            // deptView_comboBox
            // 
            this.deptView_comboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.deptView_comboBox.FormattingEnabled = true;
            this.deptView_comboBox.Items.AddRange(new object[] {
            "Alleasing",
            "Descending Order",
            "Ascending Order"});
            this.deptView_comboBox.Location = new System.Drawing.Point(8, 16);
            this.deptView_comboBox.Name = "deptView_comboBox";
            this.deptView_comboBox.Size = new System.Drawing.Size(206, 24);
            this.deptView_comboBox.TabIndex = 1;
            this.deptView_comboBox.SelectedIndexChanged += new System.EventHandler(this.deptView_comboBox_SelectedIndexChanged);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.dept_dataGridView);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBox3.Location = new System.Drawing.Point(3, 95);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(666, 308);
            this.groupBox3.TabIndex = 0;
            this.groupBox3.TabStop = false;
            // 
            // dept_dataGridView
            // 
            this.dept_dataGridView.AllowUserToAddRows = false;
            this.dept_dataGridView.AllowUserToDeleteRows = false;
            this.dept_dataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dept_dataGridView.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.dept_dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dept_dataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dept_dataGridView.Location = new System.Drawing.Point(3, 18);
            this.dept_dataGridView.Name = "dept_dataGridView";
            this.dept_dataGridView.ReadOnly = true;
            this.dept_dataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dept_dataGridView.Size = new System.Drawing.Size(660, 287);
            this.dept_dataGridView.TabIndex = 0;
            this.dept_dataGridView.SelectionChanged += new System.EventHandler(this.dept_dataGridView_SelectionChanged);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.order_combobox);
            this.tabPage3.Controls.Add(this.projectView_combobox);
            this.tabPage3.Controls.Add(this.viewProjec_button);
            this.tabPage3.Controls.Add(this.groupBox2);
            this.tabPage3.Location = new System.Drawing.Point(4, 25);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(672, 406);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "View Project";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // order_combobox
            // 
            this.order_combobox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.order_combobox.FormattingEnabled = true;
            this.order_combobox.Items.AddRange(new object[] {
            "By ID",
            "By Name"});
            this.order_combobox.Location = new System.Drawing.Point(221, 18);
            this.order_combobox.Name = "order_combobox";
            this.order_combobox.Size = new System.Drawing.Size(121, 24);
            this.order_combobox.TabIndex = 3;
            this.order_combobox.Visible = false;
            // 
            // projectView_combobox
            // 
            this.projectView_combobox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.projectView_combobox.FormattingEnabled = true;
            this.projectView_combobox.Items.AddRange(new object[] {
            "Alleasing",
            "Descending Order",
            "Ascending Order"});
            this.projectView_combobox.Location = new System.Drawing.Point(8, 18);
            this.projectView_combobox.Name = "projectView_combobox";
            this.projectView_combobox.Size = new System.Drawing.Size(207, 24);
            this.projectView_combobox.TabIndex = 2;
            this.projectView_combobox.SelectedIndexChanged += new System.EventHandler(this.projectView_combobox_SelectedIndexChanged);
            // 
            // viewProjec_button
            // 
            this.viewProjec_button.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.viewProjec_button.Location = new System.Drawing.Point(9, 48);
            this.viewProjec_button.Name = "viewProjec_button";
            this.viewProjec_button.Size = new System.Drawing.Size(116, 38);
            this.viewProjec_button.TabIndex = 1;
            this.viewProjec_button.Text = "View Project";
            this.viewProjec_button.UseVisualStyleBackColor = false;
            this.viewProjec_button.Click += new System.EventHandler(this.viewProjec_button_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.project_dataGridView);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBox2.Location = new System.Drawing.Point(3, 92);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(666, 311);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            // 
            // project_dataGridView
            // 
            this.project_dataGridView.AllowUserToAddRows = false;
            this.project_dataGridView.AllowUserToDeleteRows = false;
            this.project_dataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.project_dataGridView.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.project_dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.project_dataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.project_dataGridView.Location = new System.Drawing.Point(3, 18);
            this.project_dataGridView.Name = "project_dataGridView";
            this.project_dataGridView.ReadOnly = true;
            this.project_dataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.project_dataGridView.Size = new System.Drawing.Size(660, 290);
            this.project_dataGridView.TabIndex = 0;
            this.project_dataGridView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // ViewEmployee
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(680, 435);
            this.Controls.Add(this.tabControl1);
            this.MaximumSize = new System.Drawing.Size(696, 473);
            this.MinimumSize = new System.Drawing.Size(696, 473);
            this.Name = "ViewEmployee";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ViewEmployee";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.employee_dataGridView)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dept_dataGridView)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.project_dataGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView project_dataGridView;
        private System.Windows.Forms.Button viewProjec_button;
        private System.Windows.Forms.ComboBox projectView_combobox;
        private System.Windows.Forms.ComboBox order_combobox;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.DataGridView dept_dataGridView;
        private System.Windows.Forms.Button viewDept_button;
        private System.Windows.Forms.ComboBox orderDept_comboBox;
        private System.Windows.Forms.ComboBox deptView_comboBox;
        private System.Windows.Forms.ComboBox orderEmployee_comboBox;
        private System.Windows.Forms.ComboBox epmloyeeView_comboBox;
        private System.Windows.Forms.Button viewEmployee_button;
        private System.Windows.Forms.DataGridView employee_dataGridView;
    }
}