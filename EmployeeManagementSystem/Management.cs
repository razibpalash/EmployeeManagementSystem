﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EmployeeManagementSystem
{
    public partial class Management : Form
    {
        public Management()
        {
            InitializeComponent();
            fillSalaryDatagreadView("SELECT eid as Employee_ID, name as Name, salary as Salary from ems.employee;");
        }

        private void fillSalaryDatagreadView(string query)
        {
            string connection = "datasource = localhost; port = 3306; username = root; password = root;";
            MySqlConnection con = new MySqlConnection(connection);
            MySqlCommand commmend = new MySqlCommand(query, con);

            try
            {
                MySqlDataAdapter dataAdapter = new MySqlDataAdapter();
                dataAdapter.SelectCommand = commmend;
                DataTable dataTable = new DataTable();
                dataAdapter.Fill(dataTable);
                BindingSource bsource = new BindingSource();

                bsource.DataSource = dataTable;
                salary_dataGridView.DataSource = bsource;
                dataAdapter.Update(dataTable);
            }
            catch (Exception x)
            {
                MessageBox.Show(x.Message);
            }
        }

        private void uCase_button_Click(object sender, EventArgs e)
        {
            string query = "SELECT eid as Employee_ID, ucase(name) as Name, salary as Salary from ems.employee;";
            fillSalaryDatagreadView(query);
        }

        private void totolSalary_button_Click(object sender, EventArgs e)
        {
            string query = "select sum(salary) as Total_Salary from ems.employee;";
            fillSalaryDatagreadView(query);
        }

        private void maxSalary_button_Click(object sender, EventArgs e)
        {
            string query = "SELECT eid as Employee_ID, name as Name, salary as Salary from ems.employee where salary >= (select max(salary) from ems.employee) ;";
            fillSalaryDatagreadView(query);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            string query = "SELECT eid as Employee_ID, name as Name, salary as Salary from ems.employee where salary <= (select min(salary) from ems.employee) ;";
            fillSalaryDatagreadView(query);
        }

        private void button6_Click(object sender, EventArgs e)
        {
            string query = "SELECT avg(salary) as Avarage_Salary from ems.employee;";
            fillSalaryDatagreadView(query);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            fillSalaryDatagreadView("SELECT eid as Employee_ID, name as Name, salary as Salary from ems.employee;");
        }

    }
}
