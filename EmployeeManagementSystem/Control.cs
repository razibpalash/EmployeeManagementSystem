﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EmployeeManagementSystem
{
    public partial class ProjectControl : Form
    {
        public ProjectControl()
        {
            InitializeComponent();            
        }

        private void groupAndOrder(string query)
        {
            string connection = "datasource = localhost; port = 3306; username = root; password = root;";
            MySqlConnection con = new MySqlConnection(connection);
            MySqlCommand commmend = new MySqlCommand(query, con);

            try
            {
                MySqlDataAdapter dataAdapter = new MySqlDataAdapter();
                dataAdapter.SelectCommand = commmend;
                DataTable dataTable = new DataTable();
                dataAdapter.Fill(dataTable);
                BindingSource bsource = new BindingSource();

                bsource.DataSource = dataTable;
                control_dataGridView.DataSource = bsource;
                dataAdapter.Update(dataTable);
            }
            catch (Exception x)
            {
                MessageBox.Show(x.Message);
            }
        }

        private void join_button_Click(object sender, EventArgs e)
        {
            if (join_comboBox.Text == "Inner")
            {
                string query = "select p.pid as Project_ID, p.name as Project_Name, d.name as Department, d.did as Department_ID from ems.department as d inner join ems.projects as p where d.name = p.department;";
                groupAndOrder(query);
            }
            else if (join_comboBox.Text == "Left")
            {
                string query = "select p.pid as Project_ID, p.name as Project_name, d.name as Department, d.did as Department_ID from ems.projects as p left outer join ems.department as d on p.department = d.name;";
                groupAndOrder(query);
            }
            else if (join_comboBox.Text == "Right")
            {
                string query = "select p.pid as Project_ID, p.name as Project_name, d.name as Department, d.did as Department_ID from ems.projects as p right outer join ems.department as d on p.department = d.name;";
                groupAndOrder(query);
            }
            else if (join_comboBox.Text == "Full")
            {
                string query = "select p.pid as Project_ID, p.name as Project_name, d.name as Department, d.did as Department_ID from ems.projects as p and ems.department as d;";
                groupAndOrder(query);
            }
            else
            {
                MessageBox.Show("select join combobox");
            }
        }        

        private void group_button_Click(object sender, EventArgs e)
        {
            string query = "select p.pid as Project_ID, p.name as Project_Name, d.name as Department, d.did as Department_ID from ems.department as d inner join ems.projects as p where d.name = p.department group by p.department;";
            groupAndOrder(query);
        }

    }
}
