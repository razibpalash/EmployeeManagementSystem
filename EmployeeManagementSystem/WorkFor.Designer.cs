﻿namespace EmployeeManagementSystem
{
    partial class WorkFor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.viewEmployee_button = new System.Windows.Forms.Button();
            this.dept_comboBox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.totalEmployee_txt = new System.Windows.Forms.TextBox();
            this.employeeShow_dataGridView = new System.Windows.Forms.DataGridView();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.employeeShow_dataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.viewEmployee_button);
            this.groupBox1.Controls.Add(this.dept_comboBox);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(680, 127);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Select Departmetn";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(144, 16);
            this.label2.TabIndex = 5;
            this.label2.Text = "Select Department :";
            // 
            // viewEmployee_button
            // 
            this.viewEmployee_button.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.viewEmployee_button.Location = new System.Drawing.Point(357, 30);
            this.viewEmployee_button.Name = "viewEmployee_button";
            this.viewEmployee_button.Size = new System.Drawing.Size(131, 38);
            this.viewEmployee_button.TabIndex = 4;
            this.viewEmployee_button.Text = "View Employee";
            this.viewEmployee_button.UseVisualStyleBackColor = false;
            this.viewEmployee_button.Click += new System.EventHandler(this.viewEmployee_button_Click);
            // 
            // dept_comboBox
            // 
            this.dept_comboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.dept_comboBox.FormattingEnabled = true;
            this.dept_comboBox.Items.AddRange(new object[] {
            "C",
            "C++",
            "C#",
            "PHP",
            "JAVA",
            "WEB DESIGNER",
            "ANDROID"});
            this.dept_comboBox.Location = new System.Drawing.Point(164, 38);
            this.dept_comboBox.Name = "dept_comboBox";
            this.dept_comboBox.Size = new System.Drawing.Size(178, 24);
            this.dept_comboBox.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 402);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(126, 16);
            this.label1.TabIndex = 5;
            this.label1.Text = "Total Employee :";
            // 
            // totalEmployee_txt
            // 
            this.totalEmployee_txt.Location = new System.Drawing.Point(145, 398);
            this.totalEmployee_txt.Name = "totalEmployee_txt";
            this.totalEmployee_txt.ReadOnly = true;
            this.totalEmployee_txt.Size = new System.Drawing.Size(104, 22);
            this.totalEmployee_txt.TabIndex = 6;
            // 
            // employeeShow_dataGridView
            // 
            this.employeeShow_dataGridView.AllowUserToAddRows = false;
            this.employeeShow_dataGridView.AllowUserToDeleteRows = false;
            this.employeeShow_dataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.employeeShow_dataGridView.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.employeeShow_dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.employeeShow_dataGridView.Location = new System.Drawing.Point(0, 133);
            this.employeeShow_dataGridView.Name = "employeeShow_dataGridView";
            this.employeeShow_dataGridView.ReadOnly = true;
            this.employeeShow_dataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.employeeShow_dataGridView.Size = new System.Drawing.Size(680, 259);
            this.employeeShow_dataGridView.TabIndex = 7;
            this.employeeShow_dataGridView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.employeeShow_dataGridView_CellContentClick);
            // 
            // WorkFor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(680, 435);
            this.Controls.Add(this.employeeShow_dataGridView);
            this.Controls.Add(this.totalEmployee_txt);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(696, 473);
            this.MinimumSize = new System.Drawing.Size(696, 473);
            this.Name = "WorkFor";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Work For Employee";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.employeeShow_dataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button viewEmployee_button;
        private System.Windows.Forms.ComboBox dept_comboBox;
        private System.Windows.Forms.TextBox totalEmployee_txt;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridView employeeShow_dataGridView;
    }
}