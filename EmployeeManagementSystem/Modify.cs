﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EmployeeManagementSystem
{
    public partial class Modify : Form
    {
        public Modify()
        {
            InitializeComponent();
            AutoCompleteEmployeeTextbox();
            AutoCompleteDeptTextbox();
            AutoCompleteProjectTextbox();
        }

        private void AutoCompleteEmployeeTextbox()
        {
            searchEmployee_txt.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            searchEmployee_txt.AutoCompleteSource = AutoCompleteSource.CustomSource;
            AutoCompleteStringCollection coll = new AutoCompleteStringCollection();

            string connection = "datasource = localhost; port = 3306; username = root; password = root;";
            string query = "SELECT * FROM ems.employee;";
            MySqlConnection con = new MySqlConnection(connection);
            MySqlCommand com = new MySqlCommand(query, con);
            MySqlDataReader reader;
            try
            {
                con.Open();
                reader = com.ExecuteReader();
                while (reader.Read())
                {
                    coll.Add(reader.GetString("eid"));
                }
                con.Close();
                searchEmployee_txt.AutoCompleteCustomSource = coll;
            }
            catch (Exception x)
            {
                MessageBox.Show(x.Message);
            }
        }

        private void AutoCompleteDeptTextbox()
        {
            searchDept_txt.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            searchDept_txt.AutoCompleteSource = AutoCompleteSource.CustomSource;
            AutoCompleteStringCollection coll = new AutoCompleteStringCollection();

            string connection = "datasource = localhost; port = 3306; username = root; password = root;";
            string query = "SELECT * FROM ems.department;";
            MySqlConnection con = new MySqlConnection(connection);
            MySqlCommand com = new MySqlCommand(query, con);
            MySqlDataReader reader;
            try
            {
                con.Open();
                reader = com.ExecuteReader();
                while (reader.Read())
                {
                    coll.Add(reader.GetString("did"));
                }
                con.Close();
                searchDept_txt.AutoCompleteCustomSource = coll;
            }
            catch (Exception x)
            {
                MessageBox.Show(x.Message);
            }
        }

        private void AutoCompleteProjectTextbox()
        {
            search_txt.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            search_txt.AutoCompleteSource = AutoCompleteSource.CustomSource;
            AutoCompleteStringCollection coll = new AutoCompleteStringCollection();

            string connection = "datasource = localhost; port = 3306; username = root; password = root;";
            string query = "SELECT * FROM ems.projects;";
            MySqlConnection con = new MySqlConnection(connection);
            MySqlCommand com = new MySqlCommand(query, con);
            MySqlDataReader reader;
            try
            {
                con.Open();
                reader = com.ExecuteReader();
                while (reader.Read())
                {
                    coll.Add(reader.GetString("pid"));
                }
                con.Close();
                search_txt.AutoCompleteCustomSource = coll;
            }
            catch (Exception x)
            {
                MessageBox.Show(x.Message);
            }
        }

        private void Modify_Load(object sender, EventArgs e)
        {

        }

        private void view_button_Click(object sender, EventArgs e)
        {
            fillprojects();
            search_txt.Text = "please select project id";
            projectID_txt.ReadOnly = true;
            projectLocation_txt.ReadOnly = true;
            projectName_txt.ReadOnly = true;
            savePrject_button.Visible = false;

        }

        private void fillprojects()
        {
            string connection = "datasource = localhost; port = 3306; username = root; password = root;";
            string query = "SELECT * FROM ems.projects where pid = '" + this.search_txt.Text + "';";
            MySqlConnection con = new MySqlConnection(connection);
            MySqlCommand com = new MySqlCommand(query, con);
            MySqlDataReader reader;
            try
            {
                con.Open();
                reader = com.ExecuteReader();

                while (reader.Read())
                {
                    projectID_txt.Text = reader.GetString("pid");
                    projectName_txt.Text = reader.GetString("name");
                    deptOfPrject_txt.Text = reader.GetString("department");
                    projectLocation_txt.Text = reader.GetString("location");
                }
                con.Close();
            }
            catch (Exception x)
            {
                MessageBox.Show(x.Message);
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void search_txt_MouseClick(object sender, MouseEventArgs e)
        {
            search_txt.SelectAll();
        }

        string projectId;
        private void update_btn_Click(object sender, EventArgs e)
        {
            projectID_txt.ReadOnly = false;
            projectName_txt.ReadOnly = false;
            deptOfPrject_txt.ReadOnly = false;
            projectLocation_txt.ReadOnly = false;
            fillprojects();
            savePrject_button.Visible = true;
            savePrject_button.Text = "Save";
            projectId = search_txt.Text;
            search_txt.Text = "please select project id";
        }

        private void savePrject_button_Click(object sender, EventArgs e)
        {
            if (savePrject_button.Text == "Save")
            {
                string connection = "datasource = localhost; port = 3306; username = root; password = root;";
                string query = "update ems.projects set pid = '" + this.projectID_txt.Text + "', name = '" + this.projectName_txt.Text + "', department = '" + this.deptOfPrject_txt.Text + "', location = '" + this.projectLocation_txt.Text + "' where pid = '" + this.projectId + "';";
                MySqlConnection con = new MySqlConnection(connection);
                MySqlCommand com = new MySqlCommand(query, con);
                MySqlDataReader reader;
                try
                {
                    con.Open();
                    reader = com.ExecuteReader();
                    MessageBox.Show("Update projects");
                    con.Close();
                    projectID_txt.Text = null;
                    projectID_txt.ReadOnly = true;
                    projectLocation_txt.Text = null;
                    projectLocation_txt.ReadOnly = true;
                    projectName_txt.Text = null;
                    projectName_txt.ReadOnly = true;
                    deptOfPrject_txt.ReadOnly = true;
                    deptOfPrject_txt.Text = null;

                }
                catch (Exception x)
                {
                    MessageBox.Show(x.Message);
                }
                savePrject_button.Visible = false;
            }
            else if (savePrject_button.Text == "Delete")
            {
                string connection = "datasource = localhost; port = 3306; username = root; password = root;";
                string query = "delete from ems.projects where pid = '" + this.projectId + "';";
                MySqlConnection con = new MySqlConnection(connection);
                MySqlCommand com = new MySqlCommand(query, con);
                MySqlDataReader reader;
                try
                {
                    con.Open();
                    reader = com.ExecuteReader();
                    MessageBox.Show("Delete projects");
                    con.Close();
                    projectID_txt.Text = null;
                    projectLocation_txt.Text = null;
                    projectName_txt.Text = null;
                    deptOfPrject_txt.Text = null;
                    savePrject_button.Visible = false;
                    AutoCompleteProjectTextbox();
                }
                catch (Exception x)
                {
                    MessageBox.Show(x.Message);
                }
                
            }
            else
            {
                MessageBox.Show("please select project when you do");
            }
        }

        private void delete_button_Click(object sender, EventArgs e)
        {
            projectId = search_txt.Text;
            fillprojects();
            savePrject_button.Visible = true;
            savePrject_button.Text = "Delete";
            
            projectID_txt.ReadOnly = true;
            projectLocation_txt.ReadOnly = true;
            projectName_txt.ReadOnly = true;
            deptOfPrject_txt.ReadOnly = true;
            
            search_txt.Text = "please select project id";
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void groupBox4_Enter(object sender, EventArgs e)
        {

        }

        private void searchDept_txt_MouseClick(object sender, MouseEventArgs e)
        {
            searchDept_txt.SelectAll();
        }

        private void viewDept_button_Click(object sender, EventArgs e)
        {
            fillDepartments();
            searchDept_txt.Text = "please select department id";
            dId_txt.ReadOnly = true;
            deptName_txt.ReadOnly = true;
            dept_location_txt.ReadOnly = true;
            saveDept_btn.Visible = false;
        }

        private void fillDepartments()
        {
            string connection = "datasource = localhost; port = 3306; username = root; password = root;";
            string query = "SELECT * FROM ems.department where did = '" + this.searchDept_txt.Text + "';";
            MySqlConnection con = new MySqlConnection(connection);
            MySqlCommand com = new MySqlCommand(query, con);
            MySqlDataReader reader;
            try
            {
                con.Open();
                reader = com.ExecuteReader();

                while (reader.Read())
                {
                    dId_txt.Text = reader.GetString("did");
                    deptName_txt.Text = reader.GetString("name");
                    dept_location_txt.Text = reader.GetString("location");
                }
                con.Close();
            }
            catch (Exception x)
            {
                MessageBox.Show(x.Message);
            }
        }

        string deptId;
        private void updateDept_button_Click(object sender, EventArgs e)
        {
            dId_txt.ReadOnly = false;
            deptName_txt.ReadOnly = false;
            dept_location_txt.ReadOnly = false;
            fillDepartments();
            saveDept_btn.Visible = true;
            saveDept_btn.Text = "Save";
            deptId = searchDept_txt.Text;
            searchDept_txt.Text = "please select department id";
        }

        private void deleteDept_button_Click(object sender, EventArgs e)
        {
            deptId = searchDept_txt.Text;
            fillDepartments();
            saveDept_btn.Visible = true;
            saveDept_btn.Text = "Delete";

            dId_txt.ReadOnly = true;
            dept_location_txt.ReadOnly = true;
            deptName_txt.ReadOnly = true;

            searchDept_txt.Text = "please select department id";
        }

        private void saveDept_button_Click(object sender, EventArgs e)
        {

        }

        private void saveDept_btn_Click(object sender, EventArgs e)
        {
            if (saveDept_btn.Text == "Save")
            {
                string connection = "datasource = localhost; port = 3306; username = root; password = root;";
                string query = "update ems.department set did = '" + this.dId_txt.Text + "', name = '" + this.deptName_txt.Text + "', location = '" + this.dept_location_txt.Text + "' where did = '" + this.deptId + "';";
                MySqlConnection con = new MySqlConnection(connection);
                MySqlCommand com = new MySqlCommand(query, con);
                MySqlDataReader reader;
                try
                {
                    con.Open();
                    reader = com.ExecuteReader();
                    MessageBox.Show("Update Deptment");
                    con.Close();
                    dId_txt.Text = null;
                    dId_txt.ReadOnly = true;
                    dept_location_txt.Text = null;
                    dept_location_txt.ReadOnly = true;
                    deptName_txt.Text = null;
                    deptName_txt.ReadOnly = true;

                }
                catch (Exception x)
                {
                    MessageBox.Show(x.Message);
                }
                saveDept_btn.Visible = false;
            }
            else if (saveDept_btn.Text == "Delete")
            {
                string connection = "datasource = localhost; port = 3306; username = root; password = root;";
                string query = "delete from ems.department where did = '" + this.deptId + "';";
                MySqlConnection con = new MySqlConnection(connection);
                MySqlCommand com = new MySqlCommand(query, con);
                MySqlDataReader reader;
                try
                {
                    con.Open();
                    reader = com.ExecuteReader();
                    MessageBox.Show("Delete Department");
                    con.Close();
                    dId_txt.Text = null;
                    dept_location_txt.Text = null;
                    deptName_txt.Text = null;
                    saveDept_btn.Visible = false;
                    AutoCompleteDeptTextbox();
                }
                catch (Exception x)
                {
                    MessageBox.Show(x.Message);
                }

            }
            else
            {
                MessageBox.Show("please select depertment when you do");
            }
        }

        private void textBox1_MouseClick(object sender, MouseEventArgs e)
        {
            searchEmployee_txt.SelectAll();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void viewEmployee_button_Click(object sender, EventArgs e)
        {
            fillEmployee();
            searchEmployee_txt.Text = "please select employee id";
            viewEmployeeVisible();
            
        }

        private void viewEmployeeVisible()
        {
            eid_txt.ReadOnly = true;
            name_txt.ReadOnly = true;
            male.Enabled = false;
            female.Enabled = false;
            age_txt.ReadOnly = true;
            contacts_txt.ReadOnly = true;
            address_txt.ReadOnly = true;
            dateTimePicker1.Enabled = false;
            c_checkobx.Enabled = false;
            cSharp_checkBox.Enabled = false;
            cPush_checkBox.Enabled = false;
            java_checkBox.Enabled = false;
            php_checkBox.Enabled = false;
            android_checkBox.Enabled = false;
            webdesigner_checkBox.Enabled = false;
            salary_txt.ReadOnly = true;
            selectImage_btn.Visible = false;
            saveEmployee_button.Visible = false;
        }

        private void fillEmployee()
        {
            string connection = "datasource = localhost; port = 3306; username = root; password = root;";
            string query = "SELECT * FROM ems.employee where eid = '" + this.searchEmployee_txt.Text + "';";
            MySqlConnection con = new MySqlConnection(connection);
            MySqlCommand com = new MySqlCommand(query, con);
            MySqlDataReader reader;
            try
            {
                con.Open();
                reader = com.ExecuteReader();

                while (reader.Read())
                {
                    eid_txt.Text = reader.GetString("eid");
                    name_txt.Text = reader.GetString("name");
                    string sex = reader.GetString("sex");
                    if (sex == "Male")
                    {
                        male.Checked = true;
                    }
                    else 
                    {
                        female.Checked = true;
                    }

                    age_txt.Text = reader.GetString("age");
                    contacts_txt.Text = reader.GetString("contacts");
                    address_txt.Text = reader.GetString("address");
                    dateTimePicker1.Value = reader.GetDateTime("dateofjoining");
                    c_checkobx.Checked = reader.GetBoolean("c");
                    cSharp_checkBox.Checked = reader.GetBoolean("csharp");
                    cPush_checkBox.Checked = reader.GetBoolean("cpluse");
                    java_checkBox.Checked = reader.GetBoolean("java");
                    php_checkBox.Checked = reader.GetBoolean("php");
                    android_checkBox.Checked = reader.GetBoolean("android");
                    webdesigner_checkBox.Checked = reader.GetBoolean("webdesigner");
                    salary_txt.Text = reader.GetString("salary");

                    byte[] img = (byte[])(reader["photo"]);
                    if (img == null)
                    {
                        pictureBox1.Image = EmployeeManagementSystem.Properties.Resources.Office_Customer_Male_Light_icon;
                    }
                    else
                    {
                        pictureBox1.Image = System.Drawing.Image.FromStream(new MemoryStream(img));
                    }
                }
                con.Close();
            }
            catch (Exception x)
            {
                MessageBox.Show(x.Message);
            }
        }

        string employeeId;
        private void updateEmployee_button_Click(object sender, EventArgs e)
        {
            eid_txt.ReadOnly = false;
            name_txt.ReadOnly = false;
            male.Enabled = true;
            female.Enabled = true;
            age_txt.ReadOnly = false;
            contacts_txt.ReadOnly = false;
            address_txt.ReadOnly = false;
            dateTimePicker1.Enabled = true;
            c_checkobx.Enabled = true;
            cSharp_checkBox.Enabled = true;
            cPush_checkBox.Enabled = true;
            java_checkBox.Enabled = true;
            php_checkBox.Enabled = true;
            android_checkBox.Enabled = true;
            webdesigner_checkBox.Enabled = true;
            salary_txt.ReadOnly = false;
            selectImage_btn.Visible = true;
            saveEmployee_button.Visible = true;

            fillEmployee();
            saveEmployee_button.Visible = true;
            saveEmployee_button.Text = "Save";
            employeeId = searchEmployee_txt.Text;
            searchEmployee_txt.Text = "please select employee id";
        }

        private void deleteEmployee_button_Click(object sender, EventArgs e)
        {
            employeeId = searchEmployee_txt.Text;
            fillEmployee();
            saveEmployee_button.Text = "Delete";

            viewEmployeeVisible();
            saveEmployee_button.Visible = true;
            searchEmployee_txt.Text = "please select Employee id";
        }

        string sex;
        private string imagePath;
        private void saveEmployee_button_Click(object sender, EventArgs e)
        {
            if (saveEmployee_button.Text == "Save")
            {
                try
                {
                    byte[] imageBt = null;
                    FileStream fstream = new FileStream(this.imagePath, FileMode.Open, FileAccess.Read);
                    BinaryReader br = new BinaryReader(fstream);
                    imageBt = br.ReadBytes((int)fstream.Length);

                    string connection = "datasource = localhost; port = 3306; username = root; password = root;";
                    string query = "update ems.employee set eid = '" + this.eid_txt.Text + "',name = '" + this.name_txt.Text + "',sex = '" + this.sex + "',age = '" + this.age_txt.Text + "',contacts = '" + this.contacts_txt.Text + "',address = '" + this.address_txt.Text + "',dateofjoining = '" + this.dateTimePicker1.Text + "',c = '" + this.c_checkobx.Checked.ToString() + "',csharp = '" + this.cSharp_checkBox.Checked.ToString() + "',cpluse = '" + this.cPush_checkBox.Checked.ToString() + "',java = '" + this.java_checkBox.Checked.ToString() + "',php = '" + this.php_checkBox.Checked.ToString() + "',android = '" + this.android_checkBox.Checked.ToString() + "',webdesigner = '" + this.webdesigner_checkBox.Checked.ToString() + "',salary = '" + this.salary_txt.Text + "',photo = @IMG'" +"'where eid = '" + this.employeeId + "';";
                    MySqlConnection con = new MySqlConnection(connection);
                    MySqlCommand com = new MySqlCommand(query, con);
                    MySqlDataReader reader;
                
                    con.Open();
                    com.Parameters.Add(new MySqlParameter("@IMG", imageBt));
                    reader = com.ExecuteReader();
                    MessageBox.Show("Update Employee");
                    AllValueNull();
                    saveEmployee_button.Visible = false;
                    selectImage_btn.Visible = false;
                    con.Close();
                   

                }
                catch (Exception x)
                {
                    MessageBox.Show(x.Message);
                }
                
            }
            else if (saveEmployee_button.Text == "Delete")
            {
                string connection = "datasource = localhost; port = 3306; username = root; password = root;";
                string query = "delete from ems.employee where eid = '" + this.employeeId + "';";
                MySqlConnection con = new MySqlConnection(connection);
                MySqlCommand com = new MySqlCommand(query, con);
                MySqlDataReader reader;
                try
                {
                    con.Open();
                    reader = com.ExecuteReader();
                    MessageBox.Show("Delete Employee");
                    con.Close();
                    AutoCompleteEmployeeTextbox();
                    AllValueNull();

                }
                catch (Exception x)
                {
                    MessageBox.Show(x.Message);
                }

            }
            else
            {
                MessageBox.Show("please select when you do");
            }
        }

        private void AllValueNull()
        {
            eid_txt.Text = null;
            name_txt.Text = null;
            male.Checked = false;
            female.Checked = false;
            age_txt.Text = null;
            contacts_txt.Text = null;
            address_txt.Text = null;
            dateTimePicker1.Value = DateTime.Now;
            c_checkobx.Checked = false;
            cSharp_checkBox.Checked = false;
            cPush_checkBox.Checked = false;
            java_checkBox.Checked = false;
            php_checkBox.Checked = false;
            android_checkBox.Checked = false;
            webdesigner_checkBox.Checked = false;
            salary_txt.Text = null;
            pictureBox1.Image = EmployeeManagementSystem.Properties.Resources.Office_Customer_Male_Light_icon;
        }

        private void male_CheckedChanged(object sender, EventArgs e)
        {
            sex = "Male";
        }

        private void female_CheckedChanged(object sender, EventArgs e)
        {
            sex = "Female";
        }

        private void selectImage_btn_Click(object sender, EventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Filter = "JPG Files(*.jpg)|*.jpg|PNG Files(*.png)|*.png";
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                imagePath = dlg.FileName.ToString();
                pictureBox1.ImageLocation = imagePath;
            }
        }

        
         
    }
}
