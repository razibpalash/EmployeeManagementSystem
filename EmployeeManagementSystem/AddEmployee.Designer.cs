﻿namespace EmployeeManagementSystem
{
    partial class AddEmployee
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.java_checkBox = new System.Windows.Forms.CheckBox();
            this.c_checkobx = new System.Windows.Forms.CheckBox();
            this.android_checkBox = new System.Windows.Forms.CheckBox();
            this.php_checkBox = new System.Windows.Forms.CheckBox();
            this.webdesigner_checkBox = new System.Windows.Forms.CheckBox();
            this.cSharp_checkBox = new System.Windows.Forms.CheckBox();
            this.cPush_checkBox = new System.Windows.Forms.CheckBox();
            this.label9 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.selectImage_btn = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.address_txt = new System.Windows.Forms.TextBox();
            this.contacts_txt = new System.Windows.Forms.TextBox();
            this.salary_txt = new System.Windows.Forms.TextBox();
            this.age_txt = new System.Windows.Forms.TextBox();
            this.name_txt = new System.Windows.Forms.TextBox();
            this.eid_txt = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label11 = new System.Windows.Forms.Label();
            this.button4 = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.dept_name_txt = new System.Windows.Forms.ComboBox();
            this.dId_txt = new System.Windows.Forms.TextBox();
            this.dept_location_txt = new System.Windows.Forms.TextBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label16 = new System.Windows.Forms.Label();
            this.projectName_txt = new System.Windows.Forms.TextBox();
            this.button6 = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.button5 = new System.Windows.Forms.Button();
            this.label14 = new System.Windows.Forms.Label();
            this.projectLocation_txt = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.projectID_txt = new System.Windows.Forms.TextBox();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(680, 435);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.SystemColors.HighlightText;
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(672, 406);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Add Employee";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.java_checkBox);
            this.groupBox1.Controls.Add(this.c_checkobx);
            this.groupBox1.Controls.Add(this.android_checkBox);
            this.groupBox1.Controls.Add(this.php_checkBox);
            this.groupBox1.Controls.Add(this.webdesigner_checkBox);
            this.groupBox1.Controls.Add(this.cSharp_checkBox);
            this.groupBox1.Controls.Add(this.cPush_checkBox);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.button2);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.selectImage_btn);
            this.groupBox1.Controls.Add(this.pictureBox1);
            this.groupBox1.Controls.Add(this.radioButton2);
            this.groupBox1.Controls.Add(this.radioButton1);
            this.groupBox1.Controls.Add(this.dateTimePicker1);
            this.groupBox1.Controls.Add(this.address_txt);
            this.groupBox1.Controls.Add(this.contacts_txt);
            this.groupBox1.Controls.Add(this.salary_txt);
            this.groupBox1.Controls.Add(this.age_txt);
            this.groupBox1.Controls.Add(this.name_txt);
            this.groupBox1.Controls.Add(this.eid_txt);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(666, 400);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Employee Information";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // java_checkBox
            // 
            this.java_checkBox.AutoSize = true;
            this.java_checkBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.java_checkBox.Location = new System.Drawing.Point(337, 300);
            this.java_checkBox.Name = "java_checkBox";
            this.java_checkBox.Size = new System.Drawing.Size(57, 19);
            this.java_checkBox.TabIndex = 14;
            this.java_checkBox.Text = "JAVA";
            this.java_checkBox.UseVisualStyleBackColor = true;
            // 
            // c_checkobx
            // 
            this.c_checkobx.AutoSize = true;
            this.c_checkobx.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_checkobx.Location = new System.Drawing.Point(131, 301);
            this.c_checkobx.Name = "c_checkobx";
            this.c_checkobx.Size = new System.Drawing.Size(35, 19);
            this.c_checkobx.TabIndex = 14;
            this.c_checkobx.Text = "C";
            this.c_checkobx.UseVisualStyleBackColor = true;
            // 
            // android_checkBox
            // 
            this.android_checkBox.AutoSize = true;
            this.android_checkBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.android_checkBox.Location = new System.Drawing.Point(182, 334);
            this.android_checkBox.Name = "android_checkBox";
            this.android_checkBox.Size = new System.Drawing.Size(88, 19);
            this.android_checkBox.TabIndex = 14;
            this.android_checkBox.Text = "ANDROID";
            this.android_checkBox.UseVisualStyleBackColor = true;
            // 
            // php_checkBox
            // 
            this.php_checkBox.AutoSize = true;
            this.php_checkBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.php_checkBox.Location = new System.Drawing.Point(420, 301);
            this.php_checkBox.Name = "php_checkBox";
            this.php_checkBox.Size = new System.Drawing.Size(54, 19);
            this.php_checkBox.TabIndex = 14;
            this.php_checkBox.Text = "PHP";
            this.php_checkBox.UseVisualStyleBackColor = true;
            // 
            // webdesigner_checkBox
            // 
            this.webdesigner_checkBox.AutoSize = true;
            this.webdesigner_checkBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.webdesigner_checkBox.Location = new System.Drawing.Point(287, 334);
            this.webdesigner_checkBox.Name = "webdesigner_checkBox";
            this.webdesigner_checkBox.Size = new System.Drawing.Size(131, 19);
            this.webdesigner_checkBox.TabIndex = 14;
            this.webdesigner_checkBox.Text = "WEB DESIGNER";
            this.webdesigner_checkBox.UseVisualStyleBackColor = true;
            // 
            // cSharp_checkBox
            // 
            this.cSharp_checkBox.AutoSize = true;
            this.cSharp_checkBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cSharp_checkBox.Location = new System.Drawing.Point(182, 302);
            this.cSharp_checkBox.Name = "cSharp_checkBox";
            this.cSharp_checkBox.Size = new System.Drawing.Size(43, 19);
            this.cSharp_checkBox.TabIndex = 14;
            this.cSharp_checkBox.Text = "C#";
            this.cSharp_checkBox.UseVisualStyleBackColor = true;
            // 
            // cPush_checkBox
            // 
            this.cPush_checkBox.AutoSize = true;
            this.cPush_checkBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cPush_checkBox.Location = new System.Drawing.Point(257, 302);
            this.cPush_checkBox.Name = "cPush_checkBox";
            this.cPush_checkBox.Size = new System.Drawing.Size(51, 19);
            this.cPush_checkBox.TabIndex = 14;
            this.cPush_checkBox.Text = "C++";
            this.cPush_checkBox.UseVisualStyleBackColor = true;
            this.cPush_checkBox.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(29, 302);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(96, 16);
            this.label9.TabIndex = 13;
            this.label9.Text = "Department :";
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.button2.Location = new System.Drawing.Point(518, 346);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(116, 38);
            this.button2.TabIndex = 12;
            this.button2.Text = "Cancel";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.button1.Location = new System.Drawing.Point(518, 302);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(116, 38);
            this.button1.TabIndex = 11;
            this.button1.Text = "Save";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // selectImage_btn
            // 
            this.selectImage_btn.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.selectImage_btn.Location = new System.Drawing.Point(518, 195);
            this.selectImage_btn.Name = "selectImage_btn";
            this.selectImage_btn.Size = new System.Drawing.Size(116, 38);
            this.selectImage_btn.TabIndex = 10;
            this.selectImage_btn.Text = "Select Image";
            this.selectImage_btn.UseVisualStyleBackColor = false;
            this.selectImage_btn.Click += new System.EventHandler(this.selectImage_btn_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::EmployeeManagementSystem.Properties.Resources.Office_Customer_Male_Light_icon;
            this.pictureBox1.Location = new System.Drawing.Point(495, 39);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(165, 147);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton2.Location = new System.Drawing.Point(211, 100);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(59, 17);
            this.radioButton2.TabIndex = 4;
            this.radioButton2.TabStop = true;
            this.radioButton2.Text = "Female";
            this.radioButton2.UseVisualStyleBackColor = true;
            this.radioButton2.CheckedChanged += new System.EventHandler(this.radioButton2_CheckedChanged);
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton1.Location = new System.Drawing.Point(134, 100);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(48, 17);
            this.radioButton1.TabIndex = 3;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "Male";
            this.radioButton1.UseVisualStyleBackColor = true;
            this.radioButton1.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.CustomFormat = "yyyy-MM-dd";
            this.dateTimePicker1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker1.Location = new System.Drawing.Point(134, 267);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(344, 22);
            this.dateTimePicker1.TabIndex = 8;
            // 
            // address_txt
            // 
            this.address_txt.Location = new System.Drawing.Point(134, 195);
            this.address_txt.Multiline = true;
            this.address_txt.Name = "address_txt";
            this.address_txt.Size = new System.Drawing.Size(344, 64);
            this.address_txt.TabIndex = 7;
            // 
            // contacts_txt
            // 
            this.contacts_txt.Location = new System.Drawing.Point(134, 161);
            this.contacts_txt.Name = "contacts_txt";
            this.contacts_txt.Size = new System.Drawing.Size(344, 22);
            this.contacts_txt.TabIndex = 6;
            this.contacts_txt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.contacts_txt_KeyPress);
            // 
            // salary_txt
            // 
            this.salary_txt.Location = new System.Drawing.Point(134, 366);
            this.salary_txt.Name = "salary_txt";
            this.salary_txt.Size = new System.Drawing.Size(344, 22);
            this.salary_txt.TabIndex = 9;
            this.salary_txt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.salary_txt_KeyPress);
            // 
            // age_txt
            // 
            this.age_txt.Location = new System.Drawing.Point(134, 129);
            this.age_txt.Name = "age_txt";
            this.age_txt.Size = new System.Drawing.Size(344, 22);
            this.age_txt.TabIndex = 5;
            this.age_txt.TextChanged += new System.EventHandler(this.age_txt_TextChanged);
            this.age_txt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.age_txt_KeyPress);
            // 
            // name_txt
            // 
            this.name_txt.Location = new System.Drawing.Point(134, 70);
            this.name_txt.Name = "name_txt";
            this.name_txt.Size = new System.Drawing.Size(344, 22);
            this.name_txt.TabIndex = 2;
            // 
            // eid_txt
            // 
            this.eid_txt.Location = new System.Drawing.Point(134, 39);
            this.eid_txt.Name = "eid_txt";
            this.eid_txt.Size = new System.Drawing.Size(344, 22);
            this.eid_txt.TabIndex = 1;
            this.eid_txt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.eid_txt_KeyPress);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(62, 369);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(61, 16);
            this.label8.TabIndex = 0;
            this.label8.Text = "Salary :";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(3, 268);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(122, 16);
            this.label7.TabIndex = 0;
            this.label7.Text = "Date Of Joining :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(51, 198);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(74, 16);
            this.label6.TabIndex = 0;
            this.label6.Text = "Address :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(35, 164);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(88, 16);
            this.label5.TabIndex = 0;
            this.label5.Text = "Phone No : ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(81, 129);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(44, 16);
            this.label4.TabIndex = 0;
            this.label4.Text = "Age :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(81, 100);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(42, 16);
            this.label3.TabIndex = 0;
            this.label3.Text = "Sex :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(66, 73);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 16);
            this.label2.TabIndex = 0;
            this.label2.Text = "Name :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(18, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(105, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Employee ID :";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.groupBox2);
            this.tabPage2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(672, 406);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Add Department";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.button4);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.button3);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.dept_name_txt);
            this.groupBox2.Controls.Add(this.dId_txt);
            this.groupBox2.Controls.Add(this.dept_location_txt);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(3, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(666, 400);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Department Information";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(82, 125);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(141, 16);
            this.label11.TabIndex = 0;
            this.label11.Text = "Department Name :";
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.button4.Location = new System.Drawing.Point(457, 260);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(116, 38);
            this.button4.TabIndex = 5;
            this.button4.Text = "Cancel";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(108, 83);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(115, 16);
            this.label10.TabIndex = 0;
            this.label10.Text = "Department ID :";
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.button3.Location = new System.Drawing.Point(335, 260);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(116, 38);
            this.button3.TabIndex = 4;
            this.button3.Text = "Save";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(64, 173);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(159, 16);
            this.label12.TabIndex = 0;
            this.label12.Text = "Department Location :";
            // 
            // dept_name_txt
            // 
            this.dept_name_txt.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.dept_name_txt.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dept_name_txt.FormattingEnabled = true;
            this.dept_name_txt.Items.AddRange(new object[] {
            "C",
            "C++",
            "C#",
            "PHP",
            "JAVA",
            "WEB DESIGNER",
            "ANDROID"});
            this.dept_name_txt.Location = new System.Drawing.Point(229, 122);
            this.dept_name_txt.Name = "dept_name_txt";
            this.dept_name_txt.Size = new System.Drawing.Size(344, 24);
            this.dept_name_txt.TabIndex = 2;
            // 
            // dId_txt
            // 
            this.dId_txt.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dId_txt.Location = new System.Drawing.Point(229, 80);
            this.dId_txt.Name = "dId_txt";
            this.dId_txt.Size = new System.Drawing.Size(344, 22);
            this.dId_txt.TabIndex = 1;
            // 
            // dept_location_txt
            // 
            this.dept_location_txt.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dept_location_txt.Location = new System.Drawing.Point(229, 170);
            this.dept_location_txt.Multiline = true;
            this.dept_location_txt.Name = "dept_location_txt";
            this.dept_location_txt.Size = new System.Drawing.Size(344, 75);
            this.dept_location_txt.TabIndex = 3;
            this.dept_location_txt.Text = "Shahjadpur, Gulshan,\r\nDhaka - 1212, Bangladesh ";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.groupBox3);
            this.tabPage3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPage3.Location = new System.Drawing.Point(4, 25);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(672, 406);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Add Project";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.comboBox1);
            this.groupBox3.Controls.Add(this.label16);
            this.groupBox3.Controls.Add(this.projectName_txt);
            this.groupBox3.Controls.Add(this.button6);
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Controls.Add(this.button5);
            this.groupBox3.Controls.Add(this.label14);
            this.groupBox3.Controls.Add(this.projectLocation_txt);
            this.groupBox3.Controls.Add(this.label15);
            this.groupBox3.Controls.Add(this.projectID_txt);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.Location = new System.Drawing.Point(3, 3);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(666, 400);
            this.groupBox3.TabIndex = 6;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Project Information";
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "C",
            "C++",
            "C#",
            "PHP",
            "JAVA",
            "WEB DESIGNER",
            "ANDROID"});
            this.comboBox1.Location = new System.Drawing.Point(235, 127);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(344, 24);
            this.comboBox1.TabIndex = 7;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(133, 127);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(96, 16);
            this.label16.TabIndex = 6;
            this.label16.Text = "Department :";
            // 
            // projectName_txt
            // 
            this.projectName_txt.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.projectName_txt.Location = new System.Drawing.Point(235, 85);
            this.projectName_txt.Name = "projectName_txt";
            this.projectName_txt.Size = new System.Drawing.Size(344, 22);
            this.projectName_txt.TabIndex = 2;
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.button6.Location = new System.Drawing.Point(463, 272);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(116, 38);
            this.button6.TabIndex = 5;
            this.button6.Text = "Cancel";
            this.button6.UseVisualStyleBackColor = false;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(145, 43);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(84, 16);
            this.label13.TabIndex = 0;
            this.label13.Text = "Project ID :";
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.button5.Location = new System.Drawing.Point(337, 272);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(116, 38);
            this.button5.TabIndex = 4;
            this.button5.Text = "Save";
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(111, 88);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(118, 16);
            this.label14.TabIndex = 0;
            this.label14.Text = "Projects Name :";
            // 
            // projectLocation_txt
            // 
            this.projectLocation_txt.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.projectLocation_txt.Location = new System.Drawing.Point(235, 175);
            this.projectLocation_txt.Multiline = true;
            this.projectLocation_txt.Name = "projectLocation_txt";
            this.projectLocation_txt.Size = new System.Drawing.Size(344, 66);
            this.projectLocation_txt.TabIndex = 3;
            this.projectLocation_txt.Text = "Dhaka, Bangladesh ";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(93, 178);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(136, 16);
            this.label15.TabIndex = 0;
            this.label15.Text = "Projects Location :";
            // 
            // projectID_txt
            // 
            this.projectID_txt.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.projectID_txt.Location = new System.Drawing.Point(235, 43);
            this.projectID_txt.Name = "projectID_txt";
            this.projectID_txt.Size = new System.Drawing.Size(344, 22);
            this.projectID_txt.TabIndex = 1;
            // 
            // AddEmployee
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(680, 435);
            this.Controls.Add(this.tabControl1);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(696, 473);
            this.MinimumSize = new System.Drawing.Size(696, 473);
            this.Name = "AddEmployee";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Add Information";
            this.Load += new System.EventHandler(this.AddEmployee_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.TextBox address_txt;
        private System.Windows.Forms.TextBox contacts_txt;
        private System.Windows.Forms.TextBox age_txt;
        private System.Windows.Forms.TextBox name_txt;
        private System.Windows.Forms.TextBox eid_txt;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox salary_txt;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button selectImage_btn;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.CheckBox java_checkBox;
        private System.Windows.Forms.CheckBox c_checkobx;
        private System.Windows.Forms.CheckBox android_checkBox;
        private System.Windows.Forms.CheckBox php_checkBox;
        private System.Windows.Forms.CheckBox webdesigner_checkBox;
        private System.Windows.Forms.CheckBox cSharp_checkBox;
        private System.Windows.Forms.CheckBox cPush_checkBox;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.TextBox projectLocation_txt;
        private System.Windows.Forms.TextBox projectName_txt;
        private System.Windows.Forms.TextBox projectID_txt;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.ComboBox dept_name_txt;
        private System.Windows.Forms.TextBox dept_location_txt;
        private System.Windows.Forms.TextBox dId_txt;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label16;

    }
}