﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EmployeeManagementSystem
{
    public partial class WorkOn : Form
    {
        public WorkOn()
        {
            InitializeComponent();
            fillSelectIdCombobox();
            fillEmployeeIdCombobox();
            fillProjectIdCombobox();
        }

        private void fillSelectIdCombobox()
        {
            string connect = "datasource = localhost; port = 3306; username = root; password = root";
            string query = "select * from ems.workon;";
            MySqlConnection connection = new MySqlConnection(connect);
            MySqlCommand command = new MySqlCommand(query, connection);
            MySqlDataReader reader;

            try
            {
                connection.Open();
                reader = command.ExecuteReader();
                while (reader.Read())
                {
                    selectPid_comboBox.Items.Add(reader.GetString("pid"));
                }
            }
            catch (Exception x)
            {
                MessageBox.Show(x.Message);
            }
        }

        private void fillid()
        {
            string connect = "datasource = localhost; port = 3306; username = root; password = root";
            string query = "SELECT id from ems.workon where id = (select max(id) from ems.workon) ;";
            MySqlConnection connection = new MySqlConnection(connect);
            MySqlCommand command = new MySqlCommand(query, connection);
            MySqlDataReader reader;

            try
            {
                connection.Open();
                reader = command.ExecuteReader();
                while (reader.Read())
                {
                    id_txt.Text = (reader.GetInt32("id") + 1).ToString();
                }
            }
            catch (Exception x)
            {
                MessageBox.Show(x.Message);
            }
        }

        private void fillProjectIdCombobox()
        {
            string connect = "datasource = localhost; port = 3306; username = root; password = root";
            string query = "select * from ems.projects;";
            MySqlConnection connection = new MySqlConnection(connect);
            MySqlCommand command = new MySqlCommand(query, connection);
            MySqlDataReader reader;

            try
            {
                connection.Open();
                reader = command.ExecuteReader();
                while (reader.Read())
                {
                    pId_comboBox.Items.Add(reader.GetString("pid"));
                }
            }
            catch (Exception x)
            {
                MessageBox.Show(x.Message);
            }
        }

        private void fillEmployeeIdCombobox()
        {
            string connect = "datasource = localhost; port = 3306; username = root; password = root";
            string query = "select * from ems.employee;";
            MySqlConnection connection = new MySqlConnection(connect);
            MySqlCommand command = new MySqlCommand(query, connection);
            MySqlDataReader reader;

            try
            {
                connection.Open();
                reader = command.ExecuteReader();
                while (reader.Read())
                {
                    eid_comboBox.Items.Add(reader.GetString("eid"));
                }
            }
            catch (Exception x)
            {
                MessageBox.Show(x.Message);
            }
        }

        private void pId_comboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            fillid();
            id_txt.ReadOnly = false;
            string connect = "datasource = localhost; port = 3306; username = root; password = root";
            string query = "select * from ems.projects where pid = '" + this.pId_comboBox.Text + "';";
            MySqlConnection connection = new MySqlConnection(connect);
            MySqlCommand command = new MySqlCommand(query, connection);
            MySqlDataReader reader;

            try
            {
                connection.Open();
                reader = command.ExecuteReader();
                while (reader.Read())
                {
                    projectName_txt.Text = reader.GetString("name");
                }
            }
            catch (Exception x)
            {
                MessageBox.Show(x.Message);
            }
        }

        private void eid_comboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            id_txt.ReadOnly = true;
            string connect = "datasource = localhost; port = 3306; username = root; password = root";
            string query = "select * from ems.employee where eid = '" + this.eid_comboBox.Text + "';";
            MySqlConnection connection = new MySqlConnection(connect);
            MySqlCommand command = new MySqlCommand(query, connection);
            MySqlDataReader reader;

            try
            {
                connection.Open();
                reader = command.ExecuteReader();
                while (reader.Read())
                {
                    workOnProjectEmployee_txt.Text = workOnProjectEmployee_txt.Text + "," + reader.GetString("eid");
                }
            }
            catch (Exception x)
            {
                MessageBox.Show(x.Message);
            }
        }

        private void endDate_ValueChanged(object sender, EventArgs e) 
        {
            duration_txt.Text = (Convert.ToInt32((endDate.Value - startDate.Value).TotalDays)).ToString() + "    Days";
        }

        private void addWork_button_Click(object sender, EventArgs e)
        {
            string con = "datasource = localhost; port = 3306; username = root; password = root;";
            string Query = "insert into ems.workon values('" + this.id_txt.Text + "','" + this.pId_comboBox.Text + "','"  + this.projectName_txt.Text + "','" + this.workOnProjectEmployee_txt.Text + "','" + this.startDate.Text + "','" + this.endDate.Text + "','" + this.duration_txt.Text + "');";
            MySqlConnection connection = new MySqlConnection(con);
            MySqlCommand command = new MySqlCommand(Query, connection);
            MySqlDataReader reader;
            try
            {
                connection.Open();
                reader = command.ExecuteReader();
                MessageBox.Show("ADDED");
                nullToAll();

            }
            catch (Exception x)
            {
                MessageBox.Show(x.Message);
            }
        }

        private void nullToAll()
        {            
            pId_comboBox.Text = null;
            projectName_txt.Text = null;
            eid_comboBox.Text = null;
            workOnProjectEmployee_txt.Text = null;
            startDate.Value = DateTime.Now;
            endDate.Value = DateTime.Now;
            duration_txt.Text = null;
            id_txt.Text = null;
        }

        private void selectPid_comboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            string connect = "datasource = localhost; port = 3306; username = root; password = root";
            string query = "select * from ems.workon where pid = '" + this.selectPid_comboBox.Text + "';";
            MySqlConnection connection = new MySqlConnection(connect);
            MySqlCommand command = new MySqlCommand(query, connection);
            MySqlDataReader reader;

            try
            {
                connection.Open();
                reader = command.ExecuteReader();
                while (reader.Read())
                {
                    projectNametxt_txt.Text = reader.GetString("projectname");
                }
            }
            catch (Exception x)
            {
                MessageBox.Show(x.Message);
            }
        }

        private void tabPage1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            FindEmployee();
            
            
        }

        private void FindEmployee()
        {
            string connection = "datasource = localhost; port = 3306; username = root; password = root;";
            string query = "select * from ems.workon where pid = '" + this.selectPid_comboBox.Text + "'";
            MySqlConnection con = new MySqlConnection(connection);
            MySqlCommand commmend = new MySqlCommand(query, con);
            //MySqlDataReader reader;
            try
            {
                MySqlDataAdapter dataAdapter = new MySqlDataAdapter();
                dataAdapter.SelectCommand = commmend;
                DataTable dataTable = new DataTable();
                dataAdapter.Fill(dataTable);
                BindingSource bsource = new BindingSource();

                bsource.DataSource = dataTable;
                employeeView_dataGridView.DataSource = bsource;
                dataAdapter.Update(dataTable);
                               
                con.Close();
            }
            catch (Exception x)
            {
                MessageBox.Show(x.Message);
            }
        }
        


    }

}
