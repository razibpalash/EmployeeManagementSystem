﻿namespace EmployeeManagementSystem
{
    partial class Management
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.salary_dataGridView = new System.Windows.Forms.DataGridView();
            this.uCase_button = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.totolSalary_button = new System.Windows.Forms.Button();
            this.maxSalary_button = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.avgSalary_button = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.salary_dataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // salary_dataGridView
            // 
            this.salary_dataGridView.AllowUserToAddRows = false;
            this.salary_dataGridView.AllowUserToDeleteRows = false;
            this.salary_dataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.salary_dataGridView.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.salary_dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.salary_dataGridView.Location = new System.Drawing.Point(12, 100);
            this.salary_dataGridView.Name = "salary_dataGridView";
            this.salary_dataGridView.ReadOnly = true;
            this.salary_dataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.salary_dataGridView.Size = new System.Drawing.Size(384, 212);
            this.salary_dataGridView.TabIndex = 0;
            // 
            // uCase_button
            // 
            this.uCase_button.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.uCase_button.Location = new System.Drawing.Point(134, 56);
            this.uCase_button.Name = "uCase_button";
            this.uCase_button.Size = new System.Drawing.Size(116, 38);
            this.uCase_button.TabIndex = 1;
            this.uCase_button.Text = "Ucase Nmae";
            this.uCase_button.UseVisualStyleBackColor = false;
            this.uCase_button.Click += new System.EventHandler(this.uCase_button_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.button2.Location = new System.Drawing.Point(256, 56);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(127, 38);
            this.button2.TabIndex = 2;
            this.button2.Text = "Salary Manager";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // totolSalary_button
            // 
            this.totolSalary_button.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.totolSalary_button.Location = new System.Drawing.Point(12, 12);
            this.totolSalary_button.Name = "totolSalary_button";
            this.totolSalary_button.Size = new System.Drawing.Size(116, 38);
            this.totolSalary_button.TabIndex = 2;
            this.totolSalary_button.Text = "Totol Salary";
            this.totolSalary_button.UseVisualStyleBackColor = false;
            this.totolSalary_button.Click += new System.EventHandler(this.totolSalary_button_Click);
            // 
            // maxSalary_button
            // 
            this.maxSalary_button.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.maxSalary_button.Location = new System.Drawing.Point(256, 12);
            this.maxSalary_button.Name = "maxSalary_button";
            this.maxSalary_button.Size = new System.Drawing.Size(127, 38);
            this.maxSalary_button.TabIndex = 2;
            this.maxSalary_button.Text = "Max Salary";
            this.maxSalary_button.UseVisualStyleBackColor = false;
            this.maxSalary_button.Click += new System.EventHandler(this.maxSalary_button_Click);
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.button5.Location = new System.Drawing.Point(134, 12);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(116, 38);
            this.button5.TabIndex = 2;
            this.button5.Text = "Mini Salary";
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // avgSalary_button
            // 
            this.avgSalary_button.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.avgSalary_button.Location = new System.Drawing.Point(12, 56);
            this.avgSalary_button.Name = "avgSalary_button";
            this.avgSalary_button.Size = new System.Drawing.Size(116, 38);
            this.avgSalary_button.TabIndex = 2;
            this.avgSalary_button.Text = "Avg Salary";
            this.avgSalary_button.UseVisualStyleBackColor = false;
            this.avgSalary_button.Click += new System.EventHandler(this.button6_Click);
            // 
            // Management
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(403, 325);
            this.Controls.Add(this.avgSalary_button);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.maxSalary_button);
            this.Controls.Add(this.totolSalary_button);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.uCase_button);
            this.Controls.Add(this.salary_dataGridView);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(419, 363);
            this.MinimumSize = new System.Drawing.Size(419, 363);
            this.Name = "Management";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Salary Manager";
            ((System.ComponentModel.ISupportInitialize)(this.salary_dataGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView salary_dataGridView;
        private System.Windows.Forms.Button uCase_button;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button totolSalary_button;
        private System.Windows.Forms.Button maxSalary_button;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button avgSalary_button;
    }
}