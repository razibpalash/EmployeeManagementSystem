﻿namespace EmployeeManagementSystem
{
    partial class ProjectControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.join_button = new System.Windows.Forms.Button();
            this.group_button = new System.Windows.Forms.Button();
            this.control_dataGridView = new System.Windows.Forms.DataGridView();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.join_comboBox = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.control_dataGridView)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // join_button
            // 
            this.join_button.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.join_button.Location = new System.Drawing.Point(334, 9);
            this.join_button.Name = "join_button";
            this.join_button.Size = new System.Drawing.Size(116, 38);
            this.join_button.TabIndex = 1;
            this.join_button.Text = "Join";
            this.join_button.UseVisualStyleBackColor = false;
            this.join_button.Click += new System.EventHandler(this.join_button_Click);
            // 
            // group_button
            // 
            this.group_button.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.group_button.Location = new System.Drawing.Point(476, 9);
            this.group_button.Name = "group_button";
            this.group_button.Size = new System.Drawing.Size(116, 38);
            this.group_button.TabIndex = 2;
            this.group_button.Text = "Group By";
            this.group_button.UseVisualStyleBackColor = false;
            this.group_button.Click += new System.EventHandler(this.group_button_Click);
            // 
            // control_dataGridView
            // 
            this.control_dataGridView.AllowUserToAddRows = false;
            this.control_dataGridView.AllowUserToDeleteRows = false;
            this.control_dataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.control_dataGridView.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.control_dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.control_dataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.control_dataGridView.Location = new System.Drawing.Point(4, 19);
            this.control_dataGridView.Name = "control_dataGridView";
            this.control_dataGridView.ReadOnly = true;
            this.control_dataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.control_dataGridView.Size = new System.Drawing.Size(672, 338);
            this.control_dataGridView.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.control_dataGridView);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBox1.Location = new System.Drawing.Point(0, 74);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox1.Size = new System.Drawing.Size(680, 361);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // join_comboBox
            // 
            this.join_comboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.join_comboBox.FormattingEnabled = true;
            this.join_comboBox.Items.AddRange(new object[] {
            "Inner",
            "Left",
            "Right",
            "Full"});
            this.join_comboBox.Location = new System.Drawing.Point(103, 17);
            this.join_comboBox.Name = "join_comboBox";
            this.join_comboBox.Size = new System.Drawing.Size(204, 24);
            this.join_comboBox.TabIndex = 3;
            // 
            // ProjectControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(680, 435);
            this.Controls.Add(this.join_comboBox);
            this.Controls.Add(this.group_button);
            this.Controls.Add(this.join_button);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximumSize = new System.Drawing.Size(696, 473);
            this.MinimumSize = new System.Drawing.Size(696, 473);
            this.Name = "ProjectControl";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Project Control";
            ((System.ComponentModel.ISupportInitialize)(this.control_dataGridView)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button join_button;
        private System.Windows.Forms.Button group_button;
        private System.Windows.Forms.DataGridView control_dataGridView;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox join_comboBox;
    }
}